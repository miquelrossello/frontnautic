<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/session/data', 'Api\apiSessionController@diesReservats');
Route::get('serveis_interns/{servei_intern}/traduced/{lang}', 'Api\apiServeiInternController@traduced');
Route::get('zones/{zona}/serveis_interns', 'Api\apiZonaController@serveis_interns');
Route::get('ports/best', 'Api\apiPortController@best_port');
Route::get('ports/{port}/serveis_interns', 'Api\apiPortController@serveis_interns');
Route::get('ports/{port}/serveis_externs', 'Api\apiPortController@serveis_externs');
Route::get('ports/{port}/tipus_serveis_externs', 'Api\apiPortController@tipus_serveis_externs');
Route::get('ports/{port}/localitat', 'Api\apiPortController@localitat');
Route::get('serveis_interns/{servei_intern}/traduced', 'Api\apiServeiInternController@traduced');
Route::get('browser', 'Api\apiBrowserController@search');
Route::get('amarraments/search', 'Api\apiAmarramentController@search');
Route::get('amarraments/{amarrament}/price', 'Api\apiAmarramentController@price');
Route::get('amarraments/{amarrament}/foto', 'Api\apiAmarramentController@foto');
Route::get('topAmarres', 'Api\apiAmarramentController@topAmarres');
Route::get('reserves/{reserva}/amarrament', 'Api\apiReservaController@amarrament');
Route::get('reserves/{reserva}/vaixells', 'Api\apiReservaController@vaixells');
Route::get('reserves/{reserva}/amarrament', 'Api\apiReservaController@amarrament')->middleware('auth');
Route::get('reserves/{reserva}/client', 'Api\apiReservaController@client')->middleware('auth');
Route::get('reserves/{reserva}/propietari', 'Api\apiReservaController@propietari')->middleware('auth');
Route::get('port/lastPort/{client}','Api\apiPortController@topPortVisited');
Route::get('port/nearestPorts/lat={latitud},long={longitud},km={distancia}','Api\apiPortController@nearestPorts');
Route::get('clients/{client}/reserves', 'Api\apiClientController@reserves');
Route::get('clients/{client}/vaixells', 'Api\apiClientController@vaixells');
Route::post('session/forget/{key}', 'Api\apiSessionController@forget');
Route::post('session/{key}', 'Api\apiSessionController@store');
Route::post('reserves/{reserva}/vaixells', 'Api\apiReservaController@setVaixellToReserva')->middleware('auth');
Route::post('vaixell', 'Api\apiVaixellController@store');
Route::delete('session/{key}', 'Api\apiSessionController@delete');
Route::apiResource('amarraments', 'Api\apiAmarramentController');
Route::apiResource('ports', 'Api\apiPortController');
Route::apiResource('localitats', 'Api\apiLocalitatController');
Route::apiResource('zones', 'Api\apiZonaController');
Route::apiResource('persones', 'Api\apiPersonaController');
Route::apiResource('clients', 'Api\apiClientController');
Route::apiResource('serveis_externs', 'Api\apiServeiExternController');
Route::apiResource('session', 'Api\apiSessionController');
Route::apiResource('serveis_interns', 'Api\apiServeiInternController');
Route::apiResource('pais', 'Api\apiPaisController');
Route::apiResource('provincia', 'Api\apiProvinciaController');
Route::apiResource('reserves', 'Api\apiReservaController')->middleware('auth');
Route::apiResource('provincies', 'Api\apiProvinciaController');
Route::apiResource('propietaris', 'Api\apiPropietariController');

