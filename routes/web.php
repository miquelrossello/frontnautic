<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@landing')->name('start');

Route::get('/search', 'SearchController@index');
Route::get('/search/{amarrament}', 'SearchController@show');
Route::get('mail', 'MailController@send');
Route::resource('ports', 'PortController');
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Auth::routes();
Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('ports', 'PortController');
Route::resource('perfil', 'ClientController')->middleware('auth');
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout')->middleware('auth');
Route::get('/amarraments/{amarrament}/reserva', 'AmarramentController@reserva')->middleware('auth');
Route::resource('amarraments', 'AmarramentController');
Route::get('/identificacio', 'IdentificacioController@index')->name('authenticate');
Route::get('/footer', 'FooterController@index');
Route::resource('reserves', 'ReservaController')->middleware('auth');
