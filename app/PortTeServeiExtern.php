<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PortTeServeiExtern extends Model
{
    protected $table = 'port_te_servei_extern';
    protected $primaryKey = ['idPort', 'idServeiExtern'];
    public $timestamps = false;
}
