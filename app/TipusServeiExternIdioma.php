<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipusServeiExternIdioma extends Model
{
    protected $table = 'tipus_servei_extern_idioma';
    protected $primaryKey = ['idTipusServeiExtern', 'idIdioma'];
    public $timestamps = false;


}
