<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReservaTeVaixell extends Model
{
    protected $table = 'reserva_te_vaixell';
    protected $primaryKey = ['idReserva', 'matricula'];
    public $timestamps = false;

    protected $guarded = [];
}
