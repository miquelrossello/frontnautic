<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Port extends Model
{
    protected $table = 'port';
    protected $primaryKey = 'idPort';
    protected $hidden = array('pivot');
    public $timestamps = false;

    public static function serveis_externs($idPort, $lang) {
        return DB::table('tipus_servei_extern_idioma')
            ->join('idioma', 'tipus_servei_extern_idioma.idIdioma', '=', 'idioma.idIdioma')
            ->join('servei_extern', 'tipus_servei_extern_idioma.idTipusServeiExtern', '=', 'servei_extern.idTipusServeiExtern')
            ->join('port_te_servei_extern', 'servei_extern.idServeiExtern', '=', 'port_te_servei_extern.idServeiExtern')
            ->where('idioma.nom', '=', $lang)
            ->where('port_te_servei_extern.idPort', '=', $idPort)
            ->get();
    }

    public static function tipus_serveis_externs($idPort, $lang) {
        return DB::table('servei_extern')
            ->select('servei_extern.idTipusServeiExtern', 'tipus_servei_extern_idioma.nom', 'tipus_servei_extern.icona')
            ->rightJoin('port_te_servei_extern', 'servei_extern.idServeiExtern', '=', 'port_te_servei_extern.idServeiExtern')
            ->rightJoin('tipus_servei_extern_idioma', 'servei_extern.idTipusServeiExtern', '=', 'tipus_servei_extern_idioma.idTipusServeiExtern')
            ->rightJoin('idioma', 'idioma.idIdioma', '=', 'tipus_servei_extern_idioma.idIdioma')
            ->join('tipus_servei_extern', 'tipus_servei_extern.idTipusServeiExtern_1', '=', 'servei_extern.idTipusServeiExtern')
            ->where('port_te_servei_extern.idPort', '=', $idPort)
            ->where('idioma.nom', '=', $lang)
            ->groupBy('tipus_servei_extern_idioma.idTipusServeiExtern', 'tipus_servei_extern_idioma.nom')
            ->get();
    }

    public function localitat() {
        return $this->belongsTo(Localitat::class, 'idLocalitat', 'idLocalitat');
    }
}