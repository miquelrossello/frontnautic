<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Zona extends Model
{
    protected $table = 'zona';
    protected $primaryKey = 'idZona';
    public $timestamps = false;

    public function port() {
        return $this->belongsTo(Port::class, 'idPort', 'idPort');
    }
}
