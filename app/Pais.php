<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Pais extends Model
{
    protected $table = 'pais';
    protected $primaryKey = 'idPais';
    public $timestamps = false;

    public function provincies() {
        return $this->hasMany(Provincia::class, 'idPais');
    }

    public static function paisos() {

        return DB::table('pais')
            ->select('*')
            ->get();
    }
}
