<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Propietari extends Model
{
    protected $table = 'propietari';
    protected $primaryKey = 'idPropietari';
    public $timestamps = false;

    public function persona() {
        return $this->belongsTo(Persona::class, 'idPropietari', 'idPersona');
    }
}
