<?php

namespace App;

use Faker\Provider\DateTime;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Amarrament extends Model
{
    protected $table = 'amarrament';
    protected $primaryKey = 'idAmarrament';
    public $timestamps = false;

    public function propietari() {
        return $this->hasOne(Propietari::class, 'idPropietari', 'idPropietari');
    }

    public function zona() {
        return $this->belongsTo(Zona::class, 'idZona', 'idZona');
    }

    public function getTarifes($start, $end) {
        return DB::select('SELECT t.idTarifa, t.preu, t.data_inici, t.data_fi FROM tarifa_te_amarrament tta
                                      left join amarrament a on tta.idAmarrament = a.idAmarrament
                                      left join tarifa t on tta.idTarifa = t.idTarifa
                                 WHERE tta.idAmarrament = ' . $this->idAmarrament .'
                                   AND tta.idTarifa IN (SELECT idTarifa from tarifa WHERE
                                                             (data_inici >= "' . $start . '" AND data_inici <= "' . $end .'")
                                                               OR
                                                             (data_fi >= "' . $start . '" AND data_fi <= "' . $end .'")
                                                               OR
                                                             (data_inici >= "' . $start . '" AND data_inici <= "' . $end .'")) 
                                 AND t.data_fi > curdate() ORDER BY  t.data_inici ASC;');
    }

    public static function cardsAmarraments(){

        return DB::table('amarrament')
            ->rightJoin('zona', 'amarrament.idZona', '=', 'zona.idZona')
            ->rightJoin('port', 'zona.idPort', '=', 'port.idPort')
            ->select('amarrament.*', 'port.foto', 'port.nom')
            ->get();
    }

    public static function amarramentById($idAmarrament){

        $amarraments= DB::table('amarrament')
            ->rightJoin('zona', 'amarrament.idZona', '=', 'zona.idZona')
            ->rightJoin('port', 'zona.idPort', '=', 'port.idPort')
            ->leftJoin('propietari', 'amarrament.idPropietari', '=', 'propietari.idPropietari')
            ->select('amarrament.*', 'port.foto', 'port.nom','propietari.puntuacio_mitja')
            ->where('idAmarrament', '=', $idAmarrament)
            ->first();

        return ($amarraments);
    }

    public static function searchAmarraments($start_date, $end_date, $filterTable = null, $filterValue = null, $idAmarrament = null) {
        return DB::select("SELECT amarrament.*, port.foto, port.nom
                                FROM amarrament
                                  left JOIN no_disponibilitat_te_amarrament on amarrament.idAmarrament = no_disponibilitat_te_amarrament.idAmarrament
                                  left JOIN no_disponibilitat on no_disponibilitat_te_amarrament.idNoDisponibilitat = no_disponibilitat.idNoDisponibilitat
                                  left JOIN reserva on reserva.idAmarrament = amarrament.idAmarrament
                                  left join zona z on amarrament.idZona = z.idZona
                                  left join port on z.idPort = port.idPort
                                  left join localitat on port.idLocalitat = localitat.idLocalitat
                                  left join provincia on localitat.idProvincia = provincia.idProvincia
                                  left join pais on provincia.idPais = pais.idPais
                                WHERE amarrament.idAmarrament NOT IN (SELECT idAmarrament from no_disponibilitat_te_amarrament
                                  LEFT JOIN no_disponibilitat on no_disponibilitat_te_amarrament.idNoDisponibilitat = no_disponibilitat.idNoDisponibilitat
                                  WHERE  ('" . $start_date ."' >= no_disponibilitat.data_inici AND '" . $start_date ."' <= no_disponibilitat.data_fi) 
                                     OR ('" . $end_date ."' >= no_disponibilitat.data_inici AND '" . $end_date ."' <= no_disponibilitat.data_fi)
                                     OR ('" . $start_date . "' <= no_disponibilitat.data_inici AND '" . $end_date . "' >= no_disponibilitat.data_fi))
                                  AND amarrament.idAmarrament NOT IN (SELECT reserva.idAmarrament
                                                                    FROM reserva
                                                                    WHERE reserva.data_inici < '" . $end_date ."' AND reserva.data_fi > '" . $start_date ."')
                                  AND amarrament.disponible = 1
                                  " .  ($idAmarrament !== null ? 'AND amarrament.idAmarrament = ' . $idAmarrament : '') ."
                                  AND datediff('" . $end_date ."', CURDATE()) < amarrament.antelacio_max_reserva
                                  " . ($filterTable !== null && $filterValue !== null ? 'AND ' . $filterTable . '.nom = "' . $filterValue . '"' : '') ."
                                GROUP BY amarrament.idAmarrament ");
    }

    public function isAvailable($startDate, $endDate) {
        $availableMooring = self::searchAmarraments($startDate, $endDate, null, null, $this->idAmarrament);
        return isset($availableMooring);
    }

    //Can work better with entry model Amarrament
    public static function price(Amarrament $amarrament, $dataInici, $dataFi) {
        //print_r("Reserva->      Start: " . $dataInici . " | End: " . $dataFi . "\n");
        $tarifes = $amarrament->getTarifes($dataInici, $dataFi);
        $total_price = 0;
        $r_dI = new \DateTime($dataInici);
        $r_dF = new \DateTime($dataFi);
        if (!isset($tarifes[0])) {
           // print_r("Days duration reserva: " . date_diff($r_dI, $r_dF)->days . "\n");
            $total_price = date_diff($r_dI, $r_dF)->days * $amarrament->preu_dia;
        } else {
            $actual_date = date('Y-m-d', time());
            $last_data_fi = null;
            $cnt_tarifes = 0;
            foreach ($tarifes as $tarifa) {
                //print_r("Tarifa " . $tarifa->idTarifa ."\n");
                //print_r("Start: " . $tarifa->data_inici . " | End: " . $tarifa->data_fi . "\n");
                $dI = new \DateTime($tarifa->data_inici);
                $dF = new \DateTime($tarifa->data_fi);
                $pD = $tarifa->preu;
                if ($cnt_tarifes === 0) {
                    if ($r_dI < $dI) {
                        //print_r("Space within " . $dataInici . " and " . $tarifa->data_inici . " | " . date_diff($r_dI, $dI)->days . "\n");
                        $total_price += date_diff($r_dI, $dI)->days * $amarrament->preu_dia;
                        //print_r("Current price: " . $total_price . "\n");
                        if ($dF > $r_dF) {
                            //print_r("Price between " . $tarifa->data_inici . " and " . $dataFi . " | " . date_diff($dI, $r_dF)->days . "\n");
                            $total_price += date_diff($dI, $r_dF)->days * $pD;
                        } else {
                            //print_r("Price between " . $tarifa->data_inici . " and " . $tarifa->data_fi . " | " . date_diff($dI, $dF)->days . "\n");
                            $total_price += date_diff($dI, $dF)->days * $pD;
                        }
                        //print_r("Current price: " . $total_price . "\n");
                    } else {
                        if ($dF > $r_dF) {
                            //print_r("Price between " . $tarifa->data_inici . " and " . $dataFi . " | " . date_diff($dI, $r_dF)->days . "\n");
                            $total_price += date_diff($dI, $r_dF)->days * $pD;
                        } else {
                            //print_r("Price between " . $tarifa->data_inici . " and " . $tarifa->data_fi . " | " . date_diff($dI, $dF)->days . "\n");
                            $total_price += date_diff($dI, $dF)->days * $pD;
                        }
                    }
                } else {
                    if (date_diff(new \DateTime($last_data_fi), $dI)->days > 0) {
                        //print_r("Space within " . $last_data_fi . " and " . $tarifa->data_inici . " | " . date_diff(new \DateTime($last_data_fi), $dI)->days . "\n");
                        $total_price += date_diff(new \DateTime($last_data_fi), $dI)->days * $amarrament->preu_dia;
                       // print_r("Current price: " . $total_price . "\n");
                    }
                    if ($dF < $r_dF) {
                        //print_r("Price between " . $tarifa->data_inici . " and " . $tarifa->data_fi . " | " . date_diff($dI, $dF)->days . "\n");
                        $total_price += date_diff($dI, $dF)->days * $pD;
                    } else {
                        //print_r("Price between " . $tarifa->data_inici . " and " . $dataFi . " | " . date_diff($dI, $r_dF)->days . "\n");
                        $total_price += date_diff($dI, $r_dF)->days * $pD;
                    }
                    //print_r("Current price: " . $total_price . "\n");
                }
                $last_data_fi = $tarifa->data_fi;
                $cnt_tarifes++;
            }
        }
        //The comission is seted staticly in 5%
        $comission_percentatge = 5;
        $comission = ($total_price * $comission_percentatge) / 100;
        $no_comission_price = $total_price;
        $total_price = $total_price + $comission;
        return ['total' => number_format((double) $total_price, 2, ',', '.'),
                'price_day' => number_format((double) $total_price / date_diff($r_dI, $r_dF)->days, 2, ',', '.'),
                'comissions' => number_format((double) $comission, 2, ',', '.'),
                'total_no_comissions' => number_format((double) $no_comission_price, 2, ',', '.')];
    }
}
