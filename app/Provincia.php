<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Provincia extends Model
{
    protected $table = 'provincia';
    protected $primaryKey = 'idProvincia';
    public $timestamps = false;

    public function pais() {
        return $this->belongsTo(Pais::class, 'idPais');
    }

    public static function provinciaByPais($idPais) {

        return DB::table('provincia')
            ->select('*')
            ->where('idPais','=', $idPais)
            ->get();
    }
}
