<?php

namespace App\Http\Controllers;

use App\Amarrament;
use Illuminate\Http\Request;

class AmarramentController extends Controller
{
    public function show(Amarrament $amarrament, Request $request){
        if (str_replace(url('/'), '', url()->previous()) !== '/search') {
            return redirect('/');
        }
        return view('amarrament', compact('amarrament'));
    }

    public function reserva(Amarrament $amarrament) {
        if (session()->get('place') === null && session()->get('dataIni') === null && session()->get('dataEnd') === null
            || !$amarrament->isAvailable(session()->get('dataIni'), session()->get('dataEnd'))) {
            return redirect('/');
        }
        return view ('reserva_amarrament')->with('idAmarrament', $amarrament->idAmarrament);
    }
}