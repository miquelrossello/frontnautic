<?php

namespace App\Http\Controllers;

use App\Port;
use Illuminate\Http\Request;

class PortController extends Controller
{
    public function show(Port $port) {
        return view('port')->with(['idPort' => $port->getKey(), 'nomPort' => $port->nom]);
    }
}
