<?php

namespace App\Http\Controllers;

use App\Amarrament;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class SearchController extends Controller
{
    public function index()
    {
         $lloc=$_REQUEST['place'];
         session(['place' => $lloc]);
         $ini=$_REQUEST['dataIni'];
         session(['dataIni' => $ini]);
         $fi=$_REQUEST['dataEnd'];
         session(['dataEnd' => $fi]);
         $taula=$_REQUEST['table'];
         session(['table' => $taula]);


        return view('search');
    }
}