<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;

class ContacteController extends Controller
{

//    public function store(){
//
//
//
//    }

    public function contact(Request $request){
        $subject = "Asunto del correo";
        $for = "mooringrentals@gmail.com";
        Mail::send('email',$request->all(), function($msj) use($subject,$for){
            $msj->from("mooringrentals@gmail.com","NombreQueApareceráComoEmisor");
            $msj->subject($subject);
            $msj->to($for);
        });
        return redirect()->back();
    }


}
