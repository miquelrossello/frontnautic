<?php

namespace App\Http\Controllers;

use App\Reserva;
use Illuminate\Http\Request;

class ReservaController extends Controller
{
    public function show(Reserva $reserf) {
        return view('resume')->with('idReserva', $reserf->idReserva);
    }

    public function index(Reserva $reserva) {

        return view('reserves', compact('reserva'));
    }
}
