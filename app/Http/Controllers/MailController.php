<?php

namespace App\Http\Controllers;

use App\Mail\Register;
use function foo\func;use Illuminate\Http\Request;
use Illuminate\Mail\SendQueuedMailable;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    public function send() {
        Mail::to('rossellomelismiquel@gmail.com')->send(new Register());
    }
}
