<?php

namespace App\Http\Controllers\Auth;

use App\AuthClient;
use App\Client;
use App\Localitat;
use App\Mail\VerifyAccount;
use App\Notifications\VerifyEmail;
use App\Persona;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;



class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username' => 'required|string|max:255|unique:client',
            'password' => 'required|string|min:6|confirmed',
            'dni' => 'required|alpha_num|max:9|unique:persona',
            'nom' => 'required|string|max:255',
            'llinatge_1' => 'required|string|max:255',
            'llinatge_2' => 'string|max:255',
            'correu' => 'required|email',
            'data_naixament' => 'date',
            'telefon' => 'numeric',
            'genere' => 'string',
            'direccio' => 'string',
            'localitat' => 'string'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
       // $nomLoc=$data['localitat'];
       // $idLoc=Localitat::getIdLocByName($nomLoc);



        $persona= Persona::create([
           'dni' => $data['dni'],
           'nom' => $data['nom'],
           'llinatge_1' => $data['llinatge_1'],
           'llinatge_2' => $data['llinatge_2'],
           'correu' => $data['correu'],
           'data_naixament' => $data['data_naixament'],
           'telefon' => $data['telefon'],
           'direccio' =>$data['direccio'],
           'idLocalitat' => $data['localitat'],
           'genere' => $data['genere'],

        ]);
        AuthClient::create([
            'username' => $data['username'],
            'password' => Hash::make($data['password']),
            'idClient'=> $persona->idPersona,
            'token' => str_random(255)
        ]);

        $client =AuthClient::find($persona->idPersona);
        return $client;
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        app()->call('App\Http\Controllers\Auth\LoginController@login');

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    }
}
