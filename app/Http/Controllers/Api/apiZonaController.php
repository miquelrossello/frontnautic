<?php

namespace App\Http\Controllers\Api;

use App\ServeiIntern;
use App\Zona;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class apiZonaController extends Controller
{
    public function index() {
        return Zona::all();
    }

    public function show(Zona $zone) {
        return $zone;
    }

    public function serveis_interns(Zona $zona) {

        $lang = Input::get('lang');
        return ServeiIntern::ByZona($zona->idZona, $lang);
    }
}
