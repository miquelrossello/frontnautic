<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Propietari;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class apiPropietariController extends Controller
{
    public function show(Propietari $propietari) {
        return $propietari;
    }

}
