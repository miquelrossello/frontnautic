<?php

namespace App\Http\Controllers\Api;

use App\ServeiExtern;
use App\ServeiIntern;
use App\TipusServeiExtern;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Port;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class apiPortController extends Controller
{

    public function index()
    {
        return Port::all();
    }

    public function show(Port $port)
    {
        return $port;
    }

    public function serveis_interns(Port $port)
    {
        return ServeiIntern::byPort($port->idPort);
    }

    public function serveis_externs(Port $port, Request $request)
    {
        $lang = $request->get('lang', 'ca');
        return Port::serveis_externs($port->idPort, $lang);
    }

    public function tipus_serveis_externs(Port $port, Request $request)
    {
        $result = [];
        $idiomes = DB::table('idioma')->get();
        foreach($idiomes as $idioma) {
            $tse = Port::tipus_serveis_externs($port->idPort, $idioma->nom);
            $data = [];
            foreach ($tse as $ts) {
                $servei_extern = ServeiExtern::byIdAndLang($port->idPort, $ts->idTipusServeiExtern, $idioma->nom);
                /*if ($servei_extern->isEmpty()) {
                    $servei_extern = ServeiExtern::byIdAndLang($port->idPort, $ts->idTipusServeiExtern, 'ca');
                }*/
                array_push($data, ['title' => $servei_extern[0]->nomTipusServeiExtern, 'logo' => $ts->icona, 'services' => $servei_extern]);
            }
            $result += [$idioma->nom => $data];
        }
        return $result;
    }

    public function best_port()
    {
        return DB::table('port')
            ->select(['port.*', DB::raw('Count(*) as numRes')])
            ->join('zona', 'port.idPort', 'zona.idPort')
            ->join('amarrament', 'zona.idZona', 'amarrament.idZona')
            ->join('reserva', 'amarrament.idAmarrament', 'reserva.idAmarrament')
            ->groupBy("port.idPort")
            ->orderByDesc("numRes")
            ->limit(1)
            ->get();
    }

    public function localitat(Port $port)
    {
        return $port->localitat;
    }

    public function topPortVisited($idClient)
    {

        return DB::table('port')
            ->select(['port.nom as Port', 'reserva.data_fi as Data_Fi'])
            ->join('zona', 'port.idPort', '=', 'zona.idPort')
            ->join('amarrament', 'zona.idZona', '=', 'amarrament.idZona')
            ->join('reserva', 'amarrament.idAmarrament', '=', 'reserva.idAmarrament')
            ->join('client', 'reserva.idClient', 'client.idClient')
            ->where('client.idClient', '=', $idClient)
            ->orderBy('reserva.data_fi', 'DESC')
            ->get();

    }

    public function nearestPorts($latitud, $longitud, $radio)
    {


        return DB::table('port')
            ->select(['port.idPort as Id', 'port.nom as Puertos_Cercanos', DB::raw('(6371 * ACOS( 
                                 COS( RADIANS(' . $latitud . ') ) 
                                 * COS(RADIANS( latitud ) ) 
                                 * COS(RADIANS( longitud ) 
                                 - RADIANS(' . $longitud . ') ) 
                                 + SIN( RADIANS(' . $latitud . ') ) 
                                 * SIN(RADIANS( latitud ) ) 
                                ))AS distancia_km
                   ')])
            ->having('distancia_km', '<=', '' . $radio . '')
            ->orderBy('distancia_km', 'ASC')
            ->get();


    }

}
