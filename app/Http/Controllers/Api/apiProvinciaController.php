<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\Localitat;
use App\Provincia;


class apiProvinciaController extends Controller
{

 public function show($idProv) {

     return Localitat::getLocByProvince($idProv);
 }


}
