<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Amarrament;
use Illuminate\Support\Facades\DB;

class apiAmarramentController extends Controller
{
    public function index() {

        return Amarrament::all();
    }

    public function show($amarrament) {

        $amarre=Amarrament::amarramentById($amarrament);
        return json($amarre);

    }

    public function search() {
        $table = session('table');
        $value = session('place');
        $data_inici = session('dataIni');
        $data_fi = session('dataEnd');
        $search_result = Amarrament::searchAmarraments($data_inici, $data_fi, $table, $value);
        $final_result = array();
        foreach ($search_result as $item) {
            $item = (array) $item;
            $item['prices'] = $this->price(Amarrament::find($item['idAmarrament']));
            array_push($final_result, $item);
        }
        return $final_result;
    }

    public function topAmarres(){

        return DB::table('amarrament')
            ->select(['amarrament.nomAmarrament as Nom_Amarrament', DB::raw('Count(reserva.idReserva) as Reserves')])
            ->join('reserva', 'amarrament.idAmarrament', 'reserva.idAmarrament')
            ->groupBy('Nom_Amarrament')
            ->orderBy('Reserves', 'desc')
            ->limit('4')
            ->get();
    }

    public static function price(Amarrament $amarrament) {
        $inici=session('dataIni');
        $final=session('dataEnd');

        return Amarrament::price($amarrament, $inici, $final);
    }

    public function foto(Amarrament $amarrament) {
        return $amarrament->zona->port->foto;
    }

}
