<?php

namespace App\Http\Controllers\Api;

use App\ServeiExtern;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class apiServeiExternController extends Controller
{
    public function index() {
        return ServeiExtern::all();
    }

    public function show(ServeiExtern $serveis_extern) {
        return $serveis_extern;
    }
}
