<?php
/**
 * Created by PhpStorm.
 * User: Moha
 * Date: 21/02/2019
 * Time: 11:30
 */

namespace App\Http\Controllers\Api;

use App\Reserva;
use App\Http\Controllers\Controller;
use App\ReservaTeVaixell;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class apiReservaController extends Controller
{

    public function show(Reserva $reserf) {
        return $reserf;
    }

    public function store(Request $request) {
        if ($request->data_inici !== null && $request->data_fi !== null && $request->preu_total !== null
            && $request->idAmarrament !== null) {
            $reserva = Reserva::create($request->toArray());
            return $reserva;
        }
        return 'Can not do the reservation!';
    }

    public function setVaixellToReserva($reserva) {
        $matricula = Input::get('matricula');
        if ($matricula !== null || $matricula !== "") {
            DB::insert('insert into reserva_te_vaixell (idReserva, matricula) values (?, ?)', [$reserva, $matricula]);
        }
    }


    public function amarrament(Reserva $reserva){
        return $reserva->amarrament;
    }

    public function propietari(Reserva $reserva){
        return $reserva->amarrament->propietari->persona;
    }

    public function vaixells(Reserva $reserva){
        if ($reserva->vaixells->count() > 0) {
            return $reserva->vaixells;
        }
        return null;
    }

    public function client(Reserva $reserva) {
        return $reserva->client->idClient;
    }

}