<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Persona;

class apiPersonaController extends Controller
{
    public function index() {
        return Persona::all();
    }

    public function show($persona) {
        return Persona::findOrFail($persona);
    }
}
