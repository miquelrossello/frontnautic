<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Amarrament;

class apiBrowserController extends Controller
{


    public function search()
    {
        return DB::table('port')
            ->select(['port.idPort', 'port.nom as Port_Nom', 'localitat.idLocalitat', 'localitat.nom as Localitat_Nom', 'provincia.idProvincia', 'provincia.nom as Provincia_Nom', 'pais.idPais', 'pais.nom as Pais_Nom'])
            ->join('localitat', 'port.idLocalitat', 'localitat.idLocalitat')
            ->join('provincia', 'localitat.idProvincia', 'provincia.idProvincia')
            ->join('pais', 'provincia.idPais', 'pais.idPais')
            ->get();
    }
}