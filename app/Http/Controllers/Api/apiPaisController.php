<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\Pais;
use App\Provincia;


use Illuminate\Http\Request;

class apiPaisController extends Controller
{
    public static function index() {
        return Pais::paisos();
    }

    public function show($idPais) {

        return Provincia::provinciaByPais($idPais);

    }
}
