<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Client;
use Illuminate\Support\Facades\DB;

class apiClientController extends Controller
{

    public function index()
    {
        return abort(404);
    }

    public function show(Client $client)
    {
        return $client;
    }

    public function vaixells(Client $client)
    {
        return DB::table('vaixell')
            ->where('idPersona', '=', $client->idClient)
            ->get();
    }

    public function update($key, Request $request)
    {
        return DB::table("persona")
            ->where("idPersona", "=", $key)
            ->update([$request->column => $request->value]);
    }

    public function reserves(Client $client) {
        return $client->reserves;
    }

}
