<?php

namespace App\Http\Controllers\Api;

use App\Idioma;
use App\ServeiIntern;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class apiServeiInternController extends Controller
{
    public function index() {
        return ServeiIntern::all();
    }

    public function show(ServeiIntern $serveisIntern) {
        return $serveisIntern;
    }

}
