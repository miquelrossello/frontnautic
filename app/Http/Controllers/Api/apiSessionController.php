<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use App\Persona;
use Illuminate\Support\Facades\Session;

class apiSessionController extends Controller
{

    public function show($key)
    {
        switch ($key) {
            case "client":
                return auth()->id();
                break;
            default:
                return Session::get($key);
        }
    }

    public function delete($key)
    {
        dd($key);
    }

    public function store($key, Request $request)
    {
        Session::put($key, $request->value);
        return Session::get($key);
    }

    public function index()
    {

       return session()->all();

    }

    public function diesReservats() {

        $inici=session('dataIni');
        $final=session('dataEnd');

        $DIni = new \DateTime($inici);
        $DEnd = new \DateTime($final);

        $dies=date_diff($DIni,$DEnd);

        return $info=['inici' => $inici, 'final' => $final, 'days' => $dies->days];
    }
}
