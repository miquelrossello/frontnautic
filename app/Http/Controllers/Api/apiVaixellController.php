<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Vaixell;

class apiVaixellController extends Controller
{

    public function store(Request $request) {
        $matricula=$request->matricula;
        $nom=$request->nom;
        $eslora=$request->eslora;
        $maniga=$request->maniga;
        $calat=$request->calat;
        $idPersona=auth()->id();

        Vaixell::insertVaixell($matricula,$nom,$eslora,$maniga,$calat,$idPersona);

    }

}
