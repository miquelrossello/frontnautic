<?php

function json($data) {
    return response()->json($data, 200, [], JSON_UNESCAPED_UNICODE);
}