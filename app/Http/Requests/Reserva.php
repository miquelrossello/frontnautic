<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Session;

class Reserva extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //MUST VERIFY IF THE USER CAN MAKE THIS ACTION!!!!
        if(!auth()->check()) {
            abort('404');
            return false;
        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'data_inici' => 'required',
            'data_fi' => 'required',
            'data_reserva' => 'required',
            'comissio' => 'required',
            'preu_total' => 'required|gte:0',
            'idPoliticaCancelacio' => 'required',
            'idClient' => 'required',
            'idMetodePagament' => 'required',
            'idAmarrament' => 'required'
        ];
    }
}
