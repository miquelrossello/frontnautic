<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TipusServeiExtern extends Model
{
    protected $table = 'tipus_servei_extern';
    protected $primaryKey = 'idTipusServeiExtern_1';
    public $timestamps = false;

    public function serveis_externs() {
        return $this->hasMany(ServeiExtern::class, 'idTipusServeiExtern', 'idTipusServeiExtern_1');
    }

    public function hasDescripcio($idTipusServeiExtern, $nom) {
        if (!isset($nom) || $nom === "") {
            $nom = 'en';
        }
        $descripcions = DB::table('tipus_servei_extern_idioma')
            ->join('idioma', 'tipus_servei_extern_idioma.idIdioma', '=', 'idioma.idIdioma')
            ->join('tipus_servei_extern', 'tipus_servei_extern.idTipusServeiExtern_1', '=', 'tipus_servei_extern_idioma.idTipusServeiExtern')
            ->where('idTipusServeiExtern', '=', $idTipusServeiExtern)
            ->where('idioma.nom', '=', $nom)
            ->select('tipus_servei_extern.idTipusServeiExtern_1', 'idioma.nom', 'tipus_servei_extern_idioma.descripcio', 'tipus_servei_extern.icona', 'tipus_servei_extern.nom')
            ->get();
        return $descripcions;
    }
}
