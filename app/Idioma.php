<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Idioma extends Model
{
    protected $table = 'idioma';
    protected $primaryKey = 'idIdioma';
    public $timestamps = false;

    public function serveis_interns() {
       return $this->belongsToMany( ServeiIntern::class, 'servei_intern_idioma', 'idIdioma', 'idServeiIntern');
    }
}
