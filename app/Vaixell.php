<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Vaixell extends Model
{

    protected $table = 'vaixell';
    protected $primaryKey = 'matricula';
    public $timestamps = false;

    public static function insertVaixell($matricula,$nom,$eslora,$maniga,$calat,$idPersona) {

        DB::table('vaixell')
            ->insert(['matricula' => $matricula, 'nom' => $nom, 'eslora' => $eslora, 'maniga' => $maniga, 'calat' => $calat, 'idPersona' => $idPersona]);

    }
}
