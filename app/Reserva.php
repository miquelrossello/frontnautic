<?php

namespace App;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Reserva extends Model
{

    protected $table = 'reserva';
    protected $primaryKey = 'idReserva';
    public $timestamps = false;

    protected $guarded = [];

    public function client() {
        return $this->hasOne(Client::class, 'idClient', 'idClient');
    }

    public function amarrament() {
        return $this->hasOne(Amarrament::class, 'idAmarrament', 'idAmarrament');
    }

    public function vaixells() {
        return $this->belongsToMany(Vaixell::class, 'reserva_te_vaixell', 'idReserva', 'matricula');
    }

    public static function reservesById($idClient) {

        return DB::table('reserva')
            ->join('amarrament', 'reserva.idAmarrament' , 'amarrament.idAmarrament')
            ->join ('zona', 'amarrament.idZona', 'zona.idZona')
            ->join('port', 'zona.idPort', 'port.idPort')
            ->join('propietari', 'amarrament.idPropietari', 'propietari.idPropietari')
            ->join('persona', 'propietari.idPropietari', 'persona.idPersona')
            ->select('reserva.*', 'port.foto', 'persona.nom', 'persona.llinatge_1')
            ->where('idClient', '=', $idClient)
            ->get();
    }
}
