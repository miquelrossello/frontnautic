<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Localitat extends Model
{
    protected $table = 'localitat';
    protected $primaryKey = 'idLocalitat';
    public $timestamps = false;

    public function port() {
        return $this->hasMany(Port::class, 'idLocalitat', 'idLocalitat');
    }

    public static function getIdLocByName($name) {
        return self::where('nom', $name)->first()->idLocalitat;
    }

    public static function getLocByProvince($idProv) {

        return DB::table('localitat')
            ->select('*')
            ->where('idProvincia','=',$idProv)
            ->get();
    }
}
