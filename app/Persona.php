<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Persona extends Model
{
    use Notifiable;
    protected $table = 'persona';
    protected $primaryKey = 'idPersona';
    public $timestamps = false;

    protected $guarded = [
        'idPersona',
    ];
}
