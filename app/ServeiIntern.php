<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ServeiIntern extends Model
{

    protected $table = 'servei_intern';
    protected $primaryKey = 'idServeiIntern';
    public $timestamps = false;


    public function idiomes()
    {
        return $this->belongsToMany(Idioma::class, 'servei_intern_idioma', 'idServeiIntern', 'idIdioma');
    }

    public static function ByZona($idZona){
        $serveis = [];
        $idiomes = DB::table('idioma')
            ->get();
        foreach ($idiomes as $idioma) {
            $data = DB::table('servei_intern')
                ->join('zona_te_servei_intern', 'servei_intern.idServeiIntern', '=', 'zona_te_servei_intern.idServeiIntern')
                ->join('servei_intern_idioma','servei_intern.idServeiIntern', '=','servei_intern_idioma.idServeiIntern')
                ->join('idioma', 'idioma.idIdioma', '=', 'servei_intern_idioma.idIdioma')
                ->select('servei_intern.*','servei_intern_idioma.*','zona_te_servei_intern.idZona')
                ->where('idZona', '=', $idZona )
                ->where('idioma.nom', '=', $idioma->nom)
                ->get();
            $serveis += [$idioma->nom => $data];
        }
        return $serveis;
    }

    public static function byPort($idPort) {
        $serveis = [];
        $idiomes = DB::table('idioma')->get();
        foreach ($idiomes as $idioma) {
            $data = DB::table('port')
                ->select(DB::raw('DISTINCT zona_te_servei_intern.idServeiIntern, servei_intern_idioma.nom, servei_intern_idioma.descripcio, servei_intern.icona'))
                ->leftJoin('zona', 'port.idPort', '=', 'zona.idPort')
                ->leftJoin('zona_te_servei_intern', 'zona.idZona', '=', 'zona_te_servei_intern.idZona')
                ->leftJoin('servei_intern', 'zona_te_servei_intern.idServeiIntern', '=', 'servei_intern.idServeiIntern')
                ->leftJoin('servei_intern_idioma', 'servei_intern.idServeiIntern', '=', 'servei_intern_idioma.idServeiIntern')
                ->leftJoin('idioma', 'servei_intern_idioma.idIdioma', '=', 'idioma.idIdioma')
                ->where('zona.idPort', '=', $idPort)
                ->where('idioma.nom', '=', $idioma->nom)
                ->get();
            $serveis += [$idioma->nom => $data];
        }
        return $serveis;
    }
}
