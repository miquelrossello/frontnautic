<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ServeiExtern extends Model
{
    protected $table = 'servei_extern';
    protected $primaryKey = 'idServeiExtern';
    public $timestamps = false;

    public function port() {
        return $this->belongsToMany(Port::class, 'port_te_servei_extern', 'idPort', 'idServeiExtern');
    }

    public function tipusServeiExtern() {
        return $this->hasOne(TipusServeiExtern::class, 'idTipusServeiExtern_1', 'idTipusServeiExtern');
    }

    public static function byIdAndLang($idPort, $idServeiExtern, $lang) {
        return DB::table('servei_extern')
            ->select( 'servei_extern.idServeiExtern', 'servei_extern.nom',
                'servei_extern.correu', 'servei_extern.logotip', 'servei_extern.telefon', 'servei_extern.web',
                'tipus_servei_extern_idioma.nom as nomTipusServeiExtern', 'tipus_servei_extern_idioma.descripcio')
            ->leftJoin('port_te_servei_extern', 'servei_extern.idServeiExtern', '=', 'port_te_servei_extern.idServeiExtern')
            ->leftJoin('tipus_servei_extern', 'servei_extern.idTipusServeiExtern', '=', 'tipus_servei_extern.idTipusServeiExtern_1')
            ->leftJoin('tipus_servei_extern_idioma', 'servei_extern.idTipusServeiExtern', '=', 'tipus_servei_extern_idioma.idTipusServeiExtern')
            ->leftJoin('idioma', 'tipus_servei_extern_idioma.idIdioma', '=', 'idioma.idIdioma')
            ->where('port_te_servei_extern.idPort', '=', $idPort)
            ->where('tipus_servei_extern.idTipusServeiExtern_1', '=', $idServeiExtern)
            ->where('idioma.nom', '=', $lang)
            ->get();
    }
}
