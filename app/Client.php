<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $table = 'client';
    protected $primaryKey = 'idClient';
    public $timestamps = false;

    public function reserves() {
        return $this->hasMany(Reserva::class, 'idClient', 'idClient');
    }

}
