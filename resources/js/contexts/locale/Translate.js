import React, {Component} from "react";
import {LocaleContext} from "./LocaleContext.js";

import en from "../../../languages/en";
import es from "../../../languages/es";
import ca from "../../../languages/ca";

class Translate extends Component {

    state = {
        langs: {en, es, ca}
    };

    render() {
        const {langs} = this.state;
        const {string} = this.props;
        const {place} = this.props;
        return (
            <LocaleContext.Consumer>
                {value => langs[value.locale][place][string]}
            </LocaleContext.Consumer>
        );
    }
}

export default Translate;