import React, {Component} from 'react';
import divWithClassName from "react-bootstrap/es/utils/divWithClassName";

class Line extends Component {

    static defaultProps = {
        size: "100%"

    };

    render() {
        const {size} = this.props;
        return (
            <hr width={size}/>
        );

    }
}

export default Line;