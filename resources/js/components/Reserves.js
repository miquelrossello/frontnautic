import React, {Component} from 'react';
import Footer from './Footer/Footer'
import axios from "axios";
import {Card, CardImg, Col, Row, Button, ButtonGroup} from "reactstrap";
import Translate from "../contexts/locale/Translate";
import MyButton from "./MyButton/MyButton";
import CardReserva from "./CardReserva/CardReserva";

class Reserves extends Component {

    state = {

        reserves: null,

    };


    componentWillMount() {
        let id = document.querySelector("meta[name=id-client]").getAttribute('content');
        axios.get("/api/clients/"+ id +"/reserves").then(response => {
            this.setState({reserves: response.data});
        });

    }



    render() {
        return (
            <div>
                <h1 className="text-center"><b><Translate place={"MyCard"} string={"reserva"}/></b></h1>
                <Row>
                    <Col sm={6} className="ml-4">
                        <h4>Filtre: </h4>
                        <ButtonGroup>
                            <Button><Translate place={"Filtre"} string={"acabada"}/></Button>
                            <Button><Translate place={"Filtre"} string={"enCurs"}/></Button>
                            <Button><Translate place={"Filtre"} string={"futura"}/></Button>
                        </ButtonGroup>
                    </Col>
                </Row>
                <div className="row mt-5">
                    {this.state.reserves !== null ? this.state.reserves.map(res =>
                            <CardReserva key={res.idReserva} reserva={res.idReserva}/>
                        )
                        : ''}
                </div>
            </div>
        );
    }


}

export default Reserves;