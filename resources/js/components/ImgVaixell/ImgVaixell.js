import React, {Component} from 'react';
import './ImgVaixell.css'
import Waiting from '../Waiting/Waiting';

class ImgVaixell extends Component {
    render() {
        const style = {
            backgroundImage: 'url("/images/port/' + this.props.img + '")',
            height: "300px"
        };

        return (
            (this.props.img !== null) ?
                <div style={style} className="w-100 d-flex imgVaixell justify-content-center align-items-end">
                    {this.props.children}
                </div> : <Waiting size="50"/>
        );
    }
}

export default ImgVaixell;