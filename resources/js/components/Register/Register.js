import React, { Component } from 'react';
import {Modal, ModalHeader, ModalBody, ModalFooter, FormGroup, Col, Row } from 'reactstrap';
import MyButton from '../MyButton/MyButton';
import Inputs from '../Inputs/Inputs';
import Labels from "../Labels/Labels";
import Translate from "../../contexts/locale/Translate";
import SelectDinamics from '../SelectDinamics/SelectDinamics';





class Register extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modal: false
        };

        this.toggle = this.toggle.bind(this);
    }

    toggle = () => {
        this.setState(prevState => ({
            modal: !prevState.modal
        }));
    }



    render() {

        let token= document.querySelector("meta[name=csrf-token]").getAttribute('content');


        return (

            <div>

                <MyButton click={this.toggle} place='navbar' text="sign_up"/>
                <Modal isOpen={this.state.modal} toggle={this.toggle}>
                    <ModalHeader toggle={this.toggle}><b>Registre</b></ModalHeader>
                    <form method="POST" action="/register">
                        <input type="hidden" name="_token" value={token} />
                        <ModalBody>

                            <FormGroup>
                                <Row>
                                    <Col sm="4">
                                    <Labels id="nom" place="register" text="name"/>
                                    <Inputs tipo="text" name="nom" id="nom"/>
                                    </Col>
                                    <Col sm="4">
                                    <Labels id="llinatge_1" place="register" text="lastname1"/>
                                    <Inputs tipo="text" name="llinatge_1" id="llinatge_1"/>
                                    </Col>
                                    <Col sm="4">
                                    <Labels id="llinatge_2" place="register" text="lastname2"/>
                                    <Inputs tipo="text" name="llinatge_2" id="llinatge_2"/>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm="6">
                                        <Labels id="dni" place="register" text="dni"/>
                                        <Inputs tipo="text" name="dni" id="dni"/>
                                    </Col>
                                    <Col sm="6">
                                        <Labels id="data_naixament" place="register" text="born"/>
                                        <Inputs tipo="text" name="data_naixament" id="data_naixament"/>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm="12">
                                        <Labels id="genere" place="register" text="genere"/>
                                    </Col>
                                    <Col sm="1">
                                        <Inputs clase="ml-2" tipo="radio" name="genere" id="male" value="Masculi"/>
                                    </Col>
                                    <Col sm="1">
                                        <Translate place="register" string="male"/>
                                    </Col>
                                    <Col sm="2"></Col>
                                    <Col sm="1">
                                        <Inputs clase="ml-2" tipo="radio" name="genere" id="female" value="Femeni"/>
                                    </Col>
                                    <Col sm="1">
                                        <Translate place="register" string="female"/>
                                    </Col>
                                    <Col sm="2"></Col>
                                    <Col sm="1">
                                        <Inputs clase="ml-2" tipo="radio" name="genere" id="other" value="Altres"/>
                                    </Col>
                                    <Col sm="1">
                                        <Translate place="register" string="other"/>
                                    </Col>
                                    <Col sm="2"></Col>
                                </Row>
                                <SelectDinamics/>
                                <Row>
                                    <Col sm="6">
                                        <Labels id="direccio" place="register" text="adresa"/>
                                        <Inputs tipo="text" name="direccio" id="direccio"/>
                                    </Col>
                                    <Col sm="6">
                                        <Labels id="telefon" place="register" text="telefon"/>
                                        <Inputs tipo="text" name="telefon" id="telefon"/>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <Labels id="correu" place="register" text="correu"/>
                                        <Inputs tipo="text" name="correu" id="correu"/>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col>
                                        <Labels id="username" place="register" text="username"/>
                                        <Inputs tipo="text" name="username" id="username"/>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm="6">
                                        <Labels id="password" place="register" text="password"/>
                                        <Inputs tipo="password" name="password" id="password"/>
                                    </Col>
                                    <Col sm="6">
                                        <Labels id="password_confirmation" place="register" text="password"/>
                                        <Inputs tipo="password" name="password_confirmation" id="password_confirmation"/>
                                    </Col>
                                </Row>
                            </FormGroup>

                        </ModalBody>
                        <ModalFooter>
                            <MyButton tipo="submit" place='button' text="register"  click={this.toggle}></MyButton>{' '}
                        </ModalFooter>
                    </form>
                </Modal>
            </div>


        );

    }

}

export default Register;