import React, {Component} from 'react';
import posed from 'react-pose';
import Translate from "./../../contexts/locale/Translate";

const MoveDiv = posed.div({
    closed: {height: 0},
    open: {height: 'auto'}
});


class BoatAccordion extends Component {

    render() {
        const {boat, method, isOpen} = this.props;

        function click() {
            method(boat);
        }
        return (
            <div className="container-fluid px-3 rounded border-bottom border-light">
                <div style={{cursor: "pointer"}} onClick={this.props.toggleOpen} className="row justify-content-center">
                    <h3 className="text-center">
                        {boat.nom}
                    </h3>
                </div>
                <div className="row">
                    <MoveDiv onClick={click} className="overflow-hidden col-12 w-100 d-flex align-items-center justify-content-around"
                             pose={(isOpen) ? 'open' : 'closed'}>
                        <img width="100px" height="100px" className="rounded-circle" src="/images/system/crucero.svg" alt="poort"/>
                        <div>
                            <h5>
                                <Translate place={"vaixell"} string={"matricula"}/>:&nbsp;
                                {boat.matricula}
                            </h5>
                            <h5>
                                <Translate place={"vaixell"} string={"calat"}/>:&nbsp;
                                {boat.calat}m
                            </h5>
                            <h5>
                                <Translate place={"vaixell"} string={"eslora"}/>:&nbsp;
                                {boat.eslora}m
                            </h5>
                            <h5>
                                <Translate place={"vaixell"} string={"maniga"}/>:&nbsp;
                                {boat.maniga}m
                            </h5>
                        </div>
                    </MoveDiv>
                </div>
            </div>
        );
    }
}

export default BoatAccordion;