import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import axios from "axios";
import {ConfOfNavContext} from "../contexts/confOfNav/ConfOfNavContext";
import {LocaleContext} from '../contexts/locale/LocaleContext';
import {UserContext} from '../contexts/User/UserContext';
import Navigation from './Navigation/Navigation';
import Home from './Home';
import Detall_amarrament from './Detall_amarrament';
import Port from './Port';
import Reserva_Amarrament from './Reserva_Amarrament';
import Footer from './Footer';
import Search from './Search';
import Perfil from './Perfil';
import Identificacio from './Identificacio';
import Resume from './Resume';
import Reserves from './Reserves';

class All extends Component {

    state = {
        locale: "es",
        user: ""
    };

    changeLanguage = (newLanguage) => {
        this.setState({
            locale: newLanguage
        });
        axios.post('/api/session/lang', {value: newLanguage});

    };

    componentWillMount() {
        axios.all([

            axios.get('/api/session/person'),
            axios.get('/api/session/client'),
            axios.get('/api/session/lang'),

        ]).then(axios.spread((person, client, lang) => {
            if (person.data !== "") {
                this.setState({user: person.data});
            } else {
                this.setState({user: ""});
            }
            if (client.data !== "") {
                axios.get('/api/persones/' + client.data)
                    .then(person1 => {
                        this.setState({user: person1.data});
                        axios.post('/api/session/person', {value: person1.data});
                    });
            } else {
                this.setState({user: ""});
                axios.delete('/api/session/person');
            }
            if (lang.data !== "") {
                this.setState({
                    locale: lang.data
                });
                axios.post('/api/session/lang', {value: lang.data});
            } else {
                axios.post('/api/session/lang', {value: "es"})
            }
        }));
    }

    forceUpdateUser = (index, value) => {
        let newUser = this.state.user;
        newUser[index] = value;
        this.setState({user: newUser});
        axios.put('/api/clients/'  + this.state.user.idPersona, {column: index, value: value});
        axios.get('/api/persones/' + this.state.user.idPersona)
            .then(person1 => {
                this.setState({user: person1.data});
                axios.post('/api/session/person', {value: person1.data});
            });
    };

    render() {
        const {TheClass} = this.props;

        return (
            <LocaleContext.Provider value={{locale: this.state.locale, method: this.changeLanguage}}>
                <UserContext.Provider value={{user: this.state.user, method: this.forceUpdateUser}}>
                    <ConfOfNavContext.Provider value={{nbDark: true}}>
                        <Navigation/>
                    </ConfOfNavContext.Provider>
                    <TheClass user={this.state.user} locale={this.state.locale}/>
                </UserContext.Provider>
            </LocaleContext.Provider>
        );

    }
}

if (document.getElementById('home')) {
    ReactDOM.render(<All TheClass={Home}/>, document.getElementById('home'));
} else if (document.getElementById('detall_amarrament')) {
    ReactDOM.render(<All TheClass={Detall_amarrament}/>, document.getElementById('detall_amarrament'));
} else if (document.getElementById('port')) {
    ReactDOM.render(<All TheClass={Port}/>, document.getElementById('port'));
} else if (document.getElementById('reserva')) {
    ReactDOM.render(<All TheClass={Reserva_Amarrament}/>, document.getElementById('reserva'));
} else if (document.getElementById('search')) {
    ReactDOM.render(<All TheClass={Search}/>, document.getElementById('search'));
} else if (document.getElementById('footer')) {
    ReactDOM.render(<All TheClass={Footer}/>, document.getElementById('footer'));
} else if (document.getElementById('reserves')) {
    ReactDOM.render(<All TheClass={Reserves}/>, document.getElementById('reserves'));
} else if (document.getElementById('perfil')) {
    ReactDOM.render(<All TheClass={Perfil}/>, document.getElementById('perfil'));
} else if (document.getElementById('identificacio')) {
    ReactDOM.render(<All TheClass={Identificacio}/>, document.getElementById('identificacio'));
} else if(document.getElementById('resume')) {
    ReactDOM.render(<All TheClass={Resume}/>, document.getElementById('resume'));
} else {
    ReactDOM.render(<All TheClass={Home}/>, document.getElementById('home'));
}
