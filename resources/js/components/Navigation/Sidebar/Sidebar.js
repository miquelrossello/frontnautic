import React, {Component} from 'react';
import posed from 'react-pose';
import {ConfOfNavContext} from '../../../contexts/confOfNav/ConfOfNavContext';
import {UserContext} from '../../../contexts/User/UserContext';
import Translate from "./../../../contexts/locale/Translate";
import SelectLanguage from './../Navbar/SelectLanguage/SelectLanguage';
import AltaVaixell from "../../AltaVaixell/AltaVaixell";
import UserImg from './../../UserImg/UserImg'


const MoveDiv = posed.div({
    close: {
        width: "0px"
    },
    open: {
        width: "400px"
    },
});

class Sidebar extends Component {

    render() {

        const {stateOfNav} = this.props;
        const {toggleStateOfNav} = this.props;

        return (
            <ConfOfNavContext.Consumer>
                {value =>
                    <UserContext.Consumer>
                        {user =>
                            <MoveDiv style={{zIndex: 100, right: "0px", top: 0}}
                                     pose={stateOfNav ? 'open' : 'close'}
                                     className="position-fixed bg-dark vh-100 d-md-none overflow-hidden">
                                {user.user === "" ?
                                    <div className="h-100 w-100 d-flex flex-column align-items-center">
                                        <div style={{height: "75px"}}>

                                        </div>
                                        <button className="btn btn-primary border-0 rounded-0 w-100 mb-auto">
                                            <a className="w-100 d-flex justify-content-center py-3 text-uppercase text-light"
                                               href="/identificacio">
                                                <Translate place="navbar" string="login"/>
                                            </a>
                                        </button>
                                        <img width="220px" src="/images/system/boatDecoration2.png" alt="img M.R.S."/>

                                        <div
                                            className="mt-auto w-100 d-flex py-3 justify-content-center mb-4 text-light">
                                            <SelectLanguage/>
                                        </div>
                                    </div>
                                    :
                                    <div className="h-100 w-100 d-flex flex-column align-items-center">
                                        <div style={{height: "75px"}}>

                                        </div>
                                        <button className="btn btn-primary border-0 rounded-0 w-100 text-uppercase">

                                            <AltaVaixell/>

                                        </button>
                                        <button className="btn btn-primary border-0 rounded-0 w-100 mb-auto mb-3">
                                            <a className="w-100 d-flex justify-content-center py-3 text-uppercase text-light"
                                               href="/reserves">
                                                <Translate place="system" string="reservations"/>
                                            </a>
                                        </button>
                                        <a className="nav-item nav-link mx-3 py-0 mb-2 mt-3" href="/perfil">
                                            <UserImg img={user.user.foto} size={220}/>
                                        </a>
                                        <a className="nav-item nav-link mx-3 py-0 text-light" href="/logout">
                                            <Translate place="navbar" string="logout"/>
                                        </a>
                                        <div className="mt-auto w-100 d-flex justify-content-center mb-4 text-light">
                                            <SelectLanguage/>
                                        </div>
                                    </div>}
                            </MoveDiv>
                        }
                    </UserContext.Consumer>
                }
            </ConfOfNavContext.Consumer>
        );
    }
}

export default Sidebar;