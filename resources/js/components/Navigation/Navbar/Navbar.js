import React, {Component} from 'react';
import posed from 'react-pose';
import Translate from "./../../../contexts/locale/Translate";
import {ConfOfNavContext} from '../../../contexts/confOfNav/ConfOfNavContext';
import {UserContext} from '../../../contexts/User/UserContext';
import SelectLanguage from './SelectLanguage/SelectLanguage';
import AltaVaixell from "../../AltaVaixell/AltaVaixell";
import UserImg from './../../UserImg/UserImg'



const MoveHeader = posed.header({
    close: {
        height: "50px"
    },
    open: {
        height: "65px"
    },
});

class Navbar extends Component {

    render() {
        const {toggleStateOfNav} = this.props;
        const {maximizNav} = this.props;

        return (
            <ConfOfNavContext.Consumer>
                {value =>
                    <UserContext.Consumer>
                        {user =>
                            <MoveHeader pose={maximizNav ? 'open' : 'close'}
                                className={"fixed-top bg-primary shadow d-flex align-items-center"}>
                                <nav
                                    className={"navbar navbar-" + (value.nbDark ? "dark" : "light") + " w-100 bg-transparent px-1"}>
                                    <a className="navbar-brand py-0 mx-3 align-items-center" href="/">
                                        <h4 className="my-0">Mooring Rental Service</h4>
                                    </a>
                                    {user.user === "" ?
                                        <div className="navbar-nav d-none d-md-flex py-0 flex-row align-items-center"
                                             id="login">
                                            <a className="text-decoration-none text-light" href="/identificacio"><Translate place="navbar" string="login"/></a>
                                            <div className="nav-item mx-3 py-0 justify-content-center">
                                                <SelectLanguage/>
                                            </div>
                                        </div>
                                        :
                                        <div className="navbar-nav d-none d-md-flex py-0 flex-row align-items-center"
                                             id="login">
                                            <AltaVaixell/>
                                            <a className="nav-item nav-link mx-3 py-0 text-capitalize" href="/reserves">
                                                <Translate place="system" string="reservations"/>
                                            </a>
                                            <div className="nav-item mx-3 py-0 justify-content-center">
                                                <SelectLanguage/>
                                            </div>
                                            <a className="nav-item nav-link mx-3 py-0" href="/perfil">
                                                <UserImg img={user.user.foto} size={40}/>
                                            </a>
                                            <a className="nav-item nav-link mx-3 py-0" href="/logout">
                                                <Translate place="navbar" string="logout"/>
                                            </a>
                                        </div>}
                                    <div className="navbar-brand d-flex d-md-none align-items-center py-0 mx-3">
                                        <img onClick={toggleStateOfNav} style={{cursor: "pointer"}} height="20px"
                                             src="/images/system/menu.svg" alt="menu"/>
                                    </div>
                                </nav>
                            </MoveHeader>
                        }
                    </UserContext.Consumer>
                }
            </ConfOfNavContext.Consumer>
        );
    }
}

export default Navbar;