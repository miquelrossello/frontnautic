import React, {Component} from 'react';
import {LocaleContext} from '../../../../../contexts/locale/LocaleContext';

class Language extends Component {

    state = {
        hover: false
    };

    togglehover = () => {
        this.setState({
            hover: !this.state.hover
        });
    };

    noSelected = {
        opacity: 0.6
    };


    render() {

        const {lang} = this.props;
        const {toggleLanguage} = this.props;

        return (
            <LocaleContext.Consumer>
                {value =>
                    <a className="nav-item nav-link mx-1 py-0 d-flex align-items-center zoomXL" onClick={function () {
                        toggleLanguage();
                        value.method(lang);
                    }} href="#">
                        <img onMouseOut={this.togglehover}
                             onMouseOver={this.togglehover}
                             style={(lang !== value.locale  && !this.state.hover) ? this.noSelected : null}
                             width="20px"
                             src={"/images/system/locale/icon_" + lang + ".svg"} alt={lang}/>
                    </a>
                }
            </LocaleContext.Consumer>
        );
    }
}

export default Language;