import React, {Component} from 'react';
import {Transition} from 'react-spring'
import Translate from '../../../../contexts/locale/Translate';
import Language from './Language/Language';

class SelectLanguage extends Component {

    state = {
        desplegado: true
    };

    toggleLanguage = () => {
        this.setState({
            desplegado: !this.state.desplegado
        });
    };

    render() {

        const toggleLanguage = this.toggleLanguage;

        return (
            <Transition items={this.state.desplegado}
                        initial={null}
                        from={{opacity: 0, width: 0, height: 0}}
                        enter={{opacity: 1, width: "auto", height: "auto"}}
                        leave={{opacity: 0, width: 0, height: 0}}>
                {toggle =>
                    toggle
                        ? props => <div style={props} className="nav-item nav-link py-0 text-center"
                                        onClick={toggleLanguage}>
                        <div style={{cursor: "pointer"}}>
                            <Translate place={"language"} string={'language'}/>
                        </div>
                        </div>
                        : props =>
                            <div style={props} className="nav-item py-0 d-flex flex-row justify-content-center">
                                <Language toggleLanguage={toggleLanguage} lang="es"/>
                                <Language toggleLanguage={toggleLanguage} lang="ca"/>
                                <Language toggleLanguage={toggleLanguage} lang="en"/>
                            </div>
                }
            </Transition>
        );
    }
}

export default SelectLanguage;