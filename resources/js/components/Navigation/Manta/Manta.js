import React, {Component} from 'react';
import posed from 'react-pose';
import {Spring} from 'react-spring';
import "./manta.css";

const MoveDiv = posed.div({
    close: {
        opacity: 0,
        applyAtEnd: {display: 'none'},

    },
    open: {
        applyAtStart: {display: 'block'},
        opacity: 1,
    },
});

class Manta extends Component {

    render() {
        const {stateOfNav} = this.props;
        const {toggleStateOfNav} = this.props;

        return (
            <MoveDiv id="manta"
                     className="d-md-none"
                     pose={stateOfNav ? 'open' : 'close'}
                     onClick={toggleStateOfNav}>
            </MoveDiv>
        );
    }
}

export default Manta;