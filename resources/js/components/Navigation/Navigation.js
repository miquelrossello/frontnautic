import React, {Component} from 'react';
import {ConfOfNavContext} from '../../contexts/confOfNav/ConfOfNavContext';
import Waypoint from 'react-waypoint';
import Manta from './Manta/Manta';
import Navbar from './Navbar/Navbar';
import Sidebar from './Sidebar/Sidebar';

class Navigation extends Component {


    state = {
        stateOfNav: false,
        maximizNav: true
    };


    toggleStateOfNav = () => {
        this.setState({
            stateOfNav: !this.state.stateOfNav
        });
    };

    maximizOfNav = () => {
        this.setState({
            maximizNav: true
        });
    };

    minimizOfNav = () => {
        this.setState({
            maximizNav: false
        });
    };

    render() {
        return (
            <div>
                <Waypoint onEnter={this.maximizOfNav} onLeave={this.minimizOfNav}>
                    <div style={{height: '65px'}} className="w-100">
                    </div>
                </Waypoint>
                < Navbar toggleStateOfNav={this.toggleStateOfNav} maximizNav={this.state.maximizNav}/>
                <Manta stateOfNav={this.state.stateOfNav} toggleStateOfNav={this.toggleStateOfNav}/>
                <Sidebar stateOfNav={this.state.stateOfNav}/>
            </div>

        );
    }
}

export default Navigation;