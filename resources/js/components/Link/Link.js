import React, {Component} from 'react';

class Link extends Component {
    render() {
        return (
            <a className="text-decoration-none" href={this.props.url}>{this.props.text}</a>
        );
    }
}

export default Link;
