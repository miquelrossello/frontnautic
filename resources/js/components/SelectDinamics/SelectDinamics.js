import React, {Component} from 'react';
import axios from "axios";
import {Col, Row } from 'reactstrap';

class SelectDinamics extends Component {

    state = {
            paisos: [],
            provincies: [],
            localitats: []
        };


    componentWillMount() {
        axios.get("/api/pais").then(response =>{
            this.setState({paisos:response.data})})

    }

    changePais(e) {
        axios.get("/api/pais/"+e.target.value).then(response =>{
            this.setState({provincies:response.data})
        })

    }

    changeProvincia(e) {
        axios.get("/api/provincies/"+e.target.value).then(response =>{
            this.setState({localitats:response.data})
        })

    }

    render() {

        return (
            <Row>
            <Col sm="4">
            <select className="custom-select" name="pais" id="pais" onChange={this.changePais.bind(this)}>
                <option selected>Tria un Pais</option>
                {this.state.paisos.map(pais =>
                    <option key={pais.idPais} value={pais.idPais}>{pais.nom}</option>

                )}
            </select>
            </Col >

            <Col sm="4">
                <select className="custom-select" name="provincia" id="provincia" onChange={this.changeProvincia.bind(this)}>
                    <option selected>Tria una provincia</option>
                    {this.state.provincies.map(prov =>
                        <option key={prov.idProvincia} value={prov.idProvincia}>{prov.nom}</option>

                    )}
                </select>
            </Col>
            <Col sm="4">
                <select className="custom-select" name="localitat" id="localitat">
                    <option selected>Tria una Localitat</option>
                    {this.state.localitats.map(loc =>
                        <option key={loc.idloc} value={loc.idLocalitat}>{loc.nom}</option>

                    )}
                </select>
            </Col>
            </Row>
        );
    }
}

export default SelectDinamics;