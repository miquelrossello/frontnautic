import React, {Component} from 'react';
import Body from './Body/Body'
import Footer from './Footer/Footer'
import Waiting from './Waiting/Waiting'
import Personal from './Perfil/Personal/Personal'
import Contact from './Perfil/Contact/Contact'
import Boats from './Perfil/Boats/Boats'
import UserImg from './UserImg/UserImg'


class Perfil extends Component {

    render() {
        const {user} = this.props;

        if (user !== "") {
            return (
                <div>
                    <Body>
                    <div className="d-flex flex-column justify-content-center align-items-center rounded-circle my-4">
                        <UserImg img={user.foto} size={160}/>
                    </div>
                    <Personal/>
                    <Contact/>
                    <Boats user={user}/>
                    </Body>
                    <Footer/>
                </div>
            );
        } else {
            return (
                <div>
                    <div className="vh-100 d-flex justify-content-center align-items-center">
                        <Waiting size={300}/>
                    </div>
                    <Footer/>
                </div>
            );
        }
    }
}

export default Perfil;
