import React, {Component} from "react";


class Banner extends Component {

    static defaultProps = {
        url: "./images/system/wallpaper.jpg"
    };


    render() {

        const {url} = this.props;

        const style={
            background: 'url("' + url + '") center center no-repeat fixed',
            backgroundSize: "cover",
            top: 0
        };
        return (
            <div id="banner" style={style} className="d-flex position-absolute vh-100 w-100 justify-content-center align-items-center">
                {this.props.children}
            </div>
        );
    }
}

export default Banner;