import React, {Component} from 'react';

class Modal extends Component {

    render() {

        const {active} = this.props;

        if (!active) {

            return null;
        }


        return (

            <div>


                <div className="modal fade d-block"
                     aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLabel"></h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">

                            </div>

                        </div>
                    </div>
                </div>

            </div>


        )
            ;

    }

}

export default Modal;