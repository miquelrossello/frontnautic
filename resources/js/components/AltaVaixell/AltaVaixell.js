import React, { Component } from 'react';
import {Modal, ModalHeader, ModalBody, ModalFooter, FormGroup, Col, Row } from 'reactstrap';
import axios from 'axios';
import MyButton from '../MyButton/MyButton';
import {Input} from 'reactstrap';
import Labels from "../Labels/Labels";
import Translate from "../../contexts/locale/Translate";






class AltaVaixell extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modal: false,
            matricula: "",
            nom: "",
            eslora: "",
            maniga: "",
            calat:""
        };
    }

    toggle = () => {
        this.setState(prevState => ({
            modal: !prevState.modal
        }));
    };

    changeMatricula = (e) => {
        this.setState({matricula: e.target.value});
    };
    changeNom = (e) => {
        this.setState({nom: e.target.value});
    };
    changeEslora = (e) => {
        this.setState({eslora: e.target.value});
    };
    changeManiga = (e) => {
        this.setState({maniga: e.target.value});
    };
    changeCalat = (e) => {
        this.setState({calat: e.target.value});
    };

    eventBoto = () => {
        this.toggle();
        axios.post('/api/vaixell', {matricula: this.state.matricula, nom:this.state.nom, eslora:this.state.eslora, maniga:this.state.maniga, calat:this.state.calat})

    };



    render() {

        let token= document.querySelector("meta[name=csrf-token]").getAttribute('content');


        return (

            <div>


                <a onClick={this.toggle} style={{cursor: "pointer"}} className="nav-item nav-link"><Translate className="text-light bg-dark" place={"vaixell"} string={"uploadBoat"}/></a>
                <Modal isOpen={this.state.modal} toggle={this.toggle}>
                    <ModalHeader toggle={this.toggle}><b>Alta Vaixell</b></ModalHeader>
                    <div>
                        <input type="hidden" name="_token" value={token} />
                        <ModalBody>
                            <h1 className="text-center"><Translate place={"MyCard"} string={"alta"}/></h1>
                            <FormGroup>
                                <Row>
                                    <Col sm="6">
                                        <Labels id="matricula" place="vaixell" text="matricula"/>
                                        <Input type="text" name="matricula" id="matricula" value={this.state.matricula} onChange={this.changeMatricula}/>
                                    </Col>
                                    <Col sm="6">
                                        <Labels id="nom" place="vaixell" text="nom"/>
                                        <Input type="text" name="nom" id="nom" value={this.state.nom} onChange={this.changeNom}/>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm="4">
                                        <Labels id="eslora" place="vaixell" text="eslora"/>
                                        <Input type="text" name="eslora" id="eslora" value={this.state.eslora} onChange={this.changeEslora}/>
                                    </Col>
                                    <Col sm="4">
                                        <Labels id="maniga" place="vaixell" text="maniga"/>
                                        <Input type="text" name="maniga" id="maniga" value={this.state.maniga} onChange={this.changeManiga}/>
                                    </Col>
                                    <Col sm="4">
                                        <Labels id="calat" place="vaixell" text="calat"/>
                                        <Input type="text" name="calat" id="calat" value={this.state.calat} onChange={this.changeCalat}/>
                                    </Col>
                                </Row>
                            </FormGroup>
                        </ModalBody>
                        <ModalFooter>
                            <button className="btn btn-secondary" onClick={this.eventBoto}><Translate place={"button"} string={"submit"}/></button>
                        </ModalFooter>
                    </div>
                </Modal>
            </div>


        );

    }

}

export default AltaVaixell;