import React, { Component } from 'react';
import Translate from './../../contexts/locale/Translate';
import Overbody from "../Overbody/Overbody";
import Calendar from 'react-calendar'
import posed from 'react-pose';
import Body from "../Body/Body";
import {Col,Row,Card} from 'reactstrap';
import "./InfoAmarrament.css";
import DateAmarrament from "../DateAmarrament/DateAmarrament";
import MyButton from "../MyButton/MyButton";
import axios from "axios";


const Parent = posed.div({
    open: {
        applyAtStart: {display: "block"},
        height: "100%",
        beforeChildren: true,
    },
    close: {
        height: "0px",
        afterChildren: true,
        applyAtEnd: {display: "none"},
    },
});
const Children = posed.div({
    open: {
        opacity: 1,
    },
    close: {
        opacity: 0,
    },
});


class InfoAmarrament extends Component {


    state = {
        dateIni: new Date(),
        dateEnd: new Date(),
        isVisibleIni: false,
        isVisibleEnd: false,
        preu:[],
        dies:[]

    };


    handleChange = ({ startDate, endDate }) => {
        startDate = startDate || this.state.startDate;
        endDate = endDate || this.state.endDate;

        if (isAfter(startDate, endDate)) {
            endDate = startDate;
        }

        this.setState({ startDate, endDate });
        console.log(this.state.startDate);
        console.log(this.state.endDate);
        this.handleChange.bind(this)
    };

    handleChangeStart = startDate => this.handleChange({ startDate });

    handleChangeEnd = endDate => this.handleChange({ endDate });
    changeDate = (date) => {
        if (this.state.isVisibleIni) {
            this.setState({
                    dateIni: date
                }
            );
            this.toggleVisibilityEnd();
        } else if (this.state.isVisibleEnd) {
            this.setState({
                    dateEnd: date
                }
            );
            this.toggleVisibilityEnd();
        }
    };

    toggleVisibilityIni = () => {
        this.setState({
                isVisibleIni: !this.state.isVisibleIni,
                isVisibleEnd: false
            }
        )
    };
    toggleVisibilityEnd = () => {
        this.setState({
                isVisibleEnd: !this.state.isVisibleEnd,
                isVisibleIni: false
            }
        )
    };

    componentWillMount() {
        let id = document.querySelector("meta[name=id-amarrament]").getAttribute('content');
        axios.get("/api/amarraments/"+id+"/price").then(response => {
            this.setState({preu: response.data})
        })
        axios.get("/api/session/data").then(response => {
            console.log("Dades"+response.data)
            this.setState({dies: response.data})
        })
        axios.get('/api/session').then( ses => {
            if(ses.data.dataIni !== ""){
                const sDate = ses.data.dataIni;
                const splitDate = sDate.split("-");
                const date = new Date(splitDate[0], splitDate[1] - 1, splitDate[2]);
                this.setState({dateIni: date})
            }
            if(ses.data.dataEnd !== ""){
                const sDate = ses.data.dataEnd;
                const splitDate = sDate.split("-");
                const date = new Date(splitDate[0], splitDate[1] - 1, splitDate[2]);
                this.setState({dateEnd: date})
            }
            if(ses.data.place !== ""){
                this.setState({text: ses.data.place})
            }
            if(ses.data.table !== ""){
                this.setState({suggestionAccept: {"table": ses.data.table}})
            }
        })
    }

    setSession(){
        let valor=document.getElementsByName('dataIni').value();
        let dateI=new FormData();
        dateI.set('dateIn',valor);
        axios.post('api/session/dateIni' , {value: valor});
    }


    render() {

        const dateIni = this.state.dateIni.getDate() + " - " + (this.state.dateIni.getMonth()+1) + " - " + this.state.dateIni.getFullYear();
        const dateEnd = this.state.dateEnd.getDate() + " - " + (this.state.dateEnd.getMonth()+1) + " - " + this.state.dateEnd.getFullYear();

        console.log(this.state.preu);
        return (

            <Body>
                <Row className="mt-3">
                    <Col sm="6">
                    <h2 className="text-center"><Translate place="infoAmarrament" string="info"/></h2>
                        <Row>
                            <Col sm="12" className="mt-4">
                                <h4><b><Translate place="infoAmarrament" string="llargaria"/>: {this.props.llarg}m</b></h4>
                            </Col>
                            <Col sm="12" className="mt-4">
                                <h4><b><Translate place="infoAmarrament" string="amplada"/>: {this.props.ample}m</b></h4>
                            </Col>
                            <Col sm="12" className="mt-4">
                                <h4><b><Translate place="infoAmarrament" string="fondaria"/>: {this.props.fons}m</b></h4>
                            </Col>
                            <Col sm="12" className="mt-4">
                                <h4><b><Translate place="infoAmarrament" string="puntuacio"/>(Max 5): {this.props.puntuacio}</b></h4>
                            </Col>
                        </Row>
                    </Col>
                    <Col sm="6" className="sticky-top">

                            <Card className="m-2">
                                <Col sm="12" className="mt-3">
                                    <h3><b><Translate place="infoAmarrament" string="preuDia"/>: {this.state.preu.price_day}€</b></h3>
                                </Col>
                                <hr/>
                                <Row className="m-0">
                                <input onClick={this.toggleVisibilityIni} type="text" readOnly={true}
                                       className="form-control form-control-lg col-6   mx-auto m-1 border shadow-none p-0 text-center"
                                       name="dataIni" value={dateIni} onChange={this.setSession}>
                                </input>
                                <input onClick={this.toggleVisibilityEnd} type="text" readOnly={true}
                                       className="form-control form-control-lg col-6   mx-auto m-1 border shadow-none p-0 text-center"
                                       name="dataEnd" value={dateEnd}>
                                </input>
                                </Row>
                                <Col sm="12">
                                    <Row className="m-0">
                                    <Parent className="col p-0 m-0"
                                            pose={this.state.isVisibleIni || this.state.isVisibleEnd ? 'open' : 'close'}>
                                        <Children className="w-100 rounded-bottom">
                                            <Calendar
                                                className="w-100 rounded-bottom bg-transparent border-0 position-relative"
                                                onChange={this.changeDate}
                                                value={this.state.isVisibleIni ? this.state.dateIni : this.state.dateEnd}
                                            />
                                        </Children>
                                    </Parent>
                                    </Row>
                                </Col>
                                <hr/>
                                <Row>
                                <Col sm="6">
                                <p className="m-2">{this.state.preu.price_day}€ x {this.state.dies.days} <Translate place="infoAmarrament" string="dia"/></p>
                                </Col>
                                <Col sm="6">
                                    <p className="text-right m-2">{this.state.preu.total_no_comissions} €</p>
                                </Col>
                                    <Col sm="6">
                                        <p className="m-2"><Translate place="infoAmarrament" string="comisio"/>: 5%</p>
                                    </Col>
                                    <Col sm="6">

                                        <p className="text-right m-2">{this.state.preu.comissions} €</p>
                                    </Col>
                                    <Col sm="6">
                                        <p className="m-2">Total:</p>
                                    </Col>
                                    <Col sm="6">
                                        <p className="text-right m-2">{this.state.preu.total} €</p>
                                    </Col>
                                    <Col className="mt-5">
                                        {this.state.dateIni === null || this.state.dateEnd === null ? '' :
                                        <form method="get" action={"/amarraments/" + this.props.id + "/reserva"}>

                                            <button className="btn btn-secondary btn-block" type="submit"><h4 className="m-0"><Translate place="button" string="book"/></h4></button>

                                        </form>}
                                    </Col>
                                </Row>
                            </Card>

                    </Col>



                </Row>
            </Body>



        );

    }
}

export default InfoAmarrament;