import React, {Component} from 'react';
import Translate from "../../contexts/locale/Translate";
import Inputs from "../Inputs/Inputs";
import Select from "../Select/Select";
import Button from "../Button/Button";

class Payment extends Component {

    render() {
        const expiration_years = [];
        const months = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
        for (let years = 0; years < 5; years++) {
            expiration_years[years] = new Date().getFullYear() + years;
        }
        return (
            <div className="w-100">
                <ul className="nav nav-tabs" id="payment_tabs" role="tablist">
                    <li className="nav-item">
                        <a className="nav-link active" id="card_tab" data-toggle="tab" href="#card" role="tab" aria-controls="card" aria-selected="true">
                            <img src="/images/system/payments/master_card.svg" alt="master_card" style={{height: 35 + 'px', width: 35 + 'px'}}/>
                            <img src="/images/system/payments/visa.svg" alt="master_card" style={{height: 35 + 'px', width: 35 + 'px'}}/>
                        </a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" id="paypal_tab" data-toggle="tab" href="#paypal" role="tab" aria-controls="paypal" aria-selected="false">
                            <img src="/images/system/payments/paypal.svg" alt="master_card" style={{height: 35 + 'px', width: 35 + 'px'}}/>
                        </a>
                    </li>
                </ul>
                <div className="tab-content" id="payment_content">
                    <div className="tab-pane fade show active" id="card" role="tabpanel" aria-labelledby="card_tab">
                        <form action="" method="POST">
                            <div className="row m-2">
                                <div className="col-sm-12">
                                    <label htmlFor="card_code"><Translate place="payment" string="card_code"/></label>
                                    <Inputs clase="form-control" id="card_code" tipo="text"
                                            name="card_code" />
                                </div>
                            </div>
                            <div className="row m-2">
                                <div className="col-sm-12">
                                    <label htmlFor="month_expiration_date"><Translate place="payment"
                                                                                      string="date_expiration"/></label>
                                </div>
                            </div>
                            <div className="row m-2">
                                <div className="col-sm-6 col-6">
                                    <Select className="form-control" name="year_expiration_date" id="year_expiration_date">
                                        <option value="null">...</option>
                                        {expiration_years.map(year =>
                                            <option key={year} value={year}>{year}</option>
                                        )}
                                    </Select>
                                </div>
                                <div className="col-sm-6 col-6">
                                    <Select name="mont_expiration_date" id="month_expiration_date">
                                        <option value="null">...</option>
                                        {months.map(month =>
                                            <option key={month} value={month}>{month}</option>
                                        )}
                                    </Select>
                                </div>
                            </div>
                            <div className="row m-2">
                                <div className="col-sm-12">
                                    <label htmlFor="security_code"><Translate place="payment"
                                                                              string="security_code"/></label>
                                </div>
                            </div>
                            <div className="row m-2">
                                <div className="col-sm-4">
                                    <Inputs clase="form-control" id="security_code" tipo="text"
                                            name="security_code"/>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div className="tab-pane fade" id="paypal" role="tabpanel" aria-labelledby="paypal_tab">
                        <div className="row m-2">
                            <div className="col-sm-12">
                                <p><Translate place="system" string="not_implemented"/></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Payment;