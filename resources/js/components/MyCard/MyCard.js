import React, {Component} from 'react';
import Translate from './../../contexts/locale/Translate';

import {Card, CardImg, CardText, CardBody, CardTitle, CardSubtitle, Col, Row, CardDeck} from 'reactstrap';
import axios from "axios";


class MyCard extends Component {

    state = {
        amarraments: [],
        preu: []
    };


    componentWillMount() {

        axios.get("/api/amarraments/search").then(response => {
            this.setState({amarraments: response.data})
        })
        axios.get("/api/session/data").then(res => {
            this.setState({preu: res.data})
        })
    }

    render() {
        return (
            <Row className="m-2">{this.state.amarraments.map(amarres =>
                <Col key={amarres.idAmarrament} sm="4" className="mt-2 mb-2">
                    <a href={"/amarraments/" + amarres.idAmarrament}>
                        <Card className="h-100 zoom">
                            <CardImg top className="h-50" src={"../images/port/" + amarres.foto}
                                     alt={"Foto port " + amarres.nom}/>
                            <CardBody>
                                <CardTitle><h3><b>{amarres.nom}</b></h3></CardTitle>
                                <CardSubtitle><h5>{amarres.nomAmarrament}</h5></CardSubtitle>
                                <CardText>

                                    <Translate place={"MyCard"} string={'price'}/>: {amarres.preu_dia}
                                <br/>
                                    <Translate place={"MyCard"} string={'priceTotal'}/>: {(amarres.preu_dia)*(this.state.preu.days)}


                                </CardText>
                            </CardBody>
                        </Card>
                    </a>

                </Col>
             )}


            </Row>
        );
    }
}

export default MyCard;