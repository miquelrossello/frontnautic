import React, {Component} from 'react';
import Translate from './../../contexts/locale/Translate';
import Waiting from './../Waiting/Waiting'
import {Card, CardImg, CardText, CardBody, CardTitle, CardSubtitle} from 'reactstrap';
import axios from "axios";


class UnicCard extends Component {

    state = {
        amarrament: null
    };

    static defaultProps = {
        id: 1
    };

    componentWillMount() {
        axios.get("/api/amarraments/" + this.props.id).then(response => {
            this.setState({amarrament: response.data})
        })
    }


    render() {

        return (
            this.state.amarrament !== null ?
                <a className="my-3" href={"/amarraments/" + this.state.amarrament.idAmarrament} style={{width: "275px", height: "375px"}}>
                    <Card className="zoom h-100" >
                        <CardImg top className="h-50" src={"../images/port/" + this.state.amarrament.foto}
                                 alt={"Foto port " + this.state.amarrament.nom}/>
                        <CardBody>
                            <CardTitle><h3><b>{this.state.amarrament.nom}</b></h3></CardTitle>
                            <CardSubtitle><h5>{this.state.amarrament.nomAmarrament}</h5></CardSubtitle>
                            <CardText><Translate place={"MyCard"} string={'price'}/>:{this.state.amarrament.preu_dia}</CardText>
                        </CardBody>
                    </Card>
                </a>
                :
                <Waiting size={150}/>
        );
    }
}

export default UnicCard;