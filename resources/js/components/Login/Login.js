import React, { Component } from 'react';
import {Modal, ModalHeader, ModalBody, ModalFooter, FormGroup } from 'reactstrap';
import MyButton from '../MyButton/MyButton';
import Inputs from '../Inputs/Inputs';
import Labels from "../Labels/Labels";




class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modal: false
        };

        this.toggle = this.toggle.bind(this);
    }

    toggle = () => {
        this.setState(prevState => ({
            modal: !prevState.modal
        }));
    }



    render() {

        let token= document.querySelector("meta[name=csrf-token]").getAttribute('content');


        return (

            <div>

                <MyButton click={this.toggle} place='navbar' text="login"/>
                <Modal isOpen={this.state.modal} toggle={this.toggle}>
                    <ModalHeader toggle={this.toggle}><b>Iniciar Sessió</b></ModalHeader>
                <form method="POST" action="/login">
                    <input type="hidden" name="_token" value={token} />
                    <ModalBody>

                        <FormGroup>
                            <Labels id="username" place="login" text="username"/>
                                <Inputs tipo="text" name="username" id="username"/>
                            <Labels id="password" nom="Password" place="login" text="password"/>
                                <Inputs tipo="password" name="password" id="password"/>
                        </FormGroup>

                    </ModalBody>
                    <ModalFooter>
                        <MyButton tipo="submit" place='navbar' text="login"  click={this.toggle}></MyButton>{' '}
                    </ModalFooter>
                </form>
                </Modal>
            </div>


        );

    }

}

export default Login;

/* <a className="nav-item nav-link mx-3 py-0" href="#" click={this.toggle}>
    <Translate place={"navbar"} string={'login'}/></a>*/