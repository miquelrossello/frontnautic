import React, {Component} from "react";
import Body from '../../Body/Body';
import Overbody from '../../Overbody/Overbody';
import Translate from "../../../contexts/locale/Translate";
import Row from "react-bootstrap/es/Row";
import Col from "react-bootstrap/es/Col";
import Inputs from "../../Inputs/Inputs";
import FormGroup from "react-bootstrap/es/FormGroup";
import Labels from "../../Labels/Labels";
import MyButton from "../../MyButton/MyButton";



class Contacte extends Component {

    state = {



    }

    render() {
        return (

            <Body>
            <div className="row my-4 d-flex justify-content-center">
                <div
                    className="col-11 col-md-9 bg-white shadow-lg rounded d-flex pl-5 justify-content-center align-items-center">

                    <form action="" method="POST" className="w-60 px-3 py-3">


                        <FormGroup>

                            <Row>

                                <Col lg="4">


                                    <Labels id="nombre" place={"footerPages"} text={"Name:"}/>
                                    <Inputs name="name" tipo="text"/>

                                </Col>

                                <Col lg="4">


                                    <Labels place={"footerPages"} text={"First Surname:"}/>

                                    <Inputs name="surname1" tipo="text"/>

                                </Col>

                                <Col lg="4">

                                    <Labels place={"footerPages"} text={"Last Surname:"}/>
                                    <Inputs name="surname2" tipo="text"/>
                                </Col>
                            </Row>

                            <Labels place={"footerPages"} text={"Email:"}/>

                            <Inputs name="email" tipo="text"/>


                            <Labels place={"footerPages"} text={"Phone Number:"}/>
                            <Inputs name="phone" tipo="text"/>

                            <Labels place={"footerPages"} text={"Subject:"}/>
                            <Inputs name="subject" tipo="text"/>

                            <Labels id="mensaje" place={"footerPages"} text={"Message:"}/>
                            <textarea className="form-control" name="message" id="" cols="30" rows="10"></textarea>




                        </FormGroup>
                        <MyButton place="footerPages" text="Submit" bloc="float-right mt-1"/>
                    </form>


                </div>
            </div>
            </Body>
        );
    }
}

export default Contacte;