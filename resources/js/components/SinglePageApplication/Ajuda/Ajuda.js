import React, {Component} from "react";
import Body from "../../Body/Body";
import Overbody from "../../Overbody/Overbody";
import AltaVaixell from "../../AltaVaixell/AltaVaixell";
import Translate from "../../../contexts/locale/Translate";
import posed from 'react-pose';
import Card from "react-bootstrap/Card";
import CardText from "reactstrap/es/CardText";
import CardBody from "reactstrap/es/CardBody";
import CardTitle from "reactstrap/es/CardTitle";

const Content = posed.div({
    closed: {height: 0},
    open: {height: 'auto'}
});

class Ajuda extends Component {

    state = {
        openDescription: false,
        openReserve: false,
        openProfile: false,
        upBoat: false,
        openBoat: false

    };


    toggleOpenDescription = () => {

        this.setState({openDescription: !this.state.openDescription})

    }

    toggleOpenReserve = () => {

        this.setState({openReserve: !this.state.openReserve})

    }

    toggleOpenProfile = () => {

        this.setState({openProfile: !this.state.openProfile})

    }

    toggleOpenBoat = () => {

        this.setState({openBoat: !this.state.openBoat})

    }

    render() {

        const {openDescription} = this.state;
        const {openReserve} = this.state;
        const {openProfile} = this.state;
        const {openBoat} = this.state;

        return (
            <Body>
            <Overbody>
                <div className="py-5 d-flex flex-column align-items-center">


                    <h3><Translate place={"footerPages"} string={"Hello, what can we help you?"}/></h3>

                    <Card>
                        <CardBody>
                            <CardTitle className="mb-4">
                                <h5 className="title text-center" style={{cursor: "pointer"}}
                                    onClick={this.toggleOpenDescription}><Translate place={"footerPages"}
                                                                                    string={"How to search?"}/>

                                    {!openDescription ? ' ▼ ' : ' ▲ '}
                                </h5>
                            </CardTitle>


                            <Content className="content overflow-hidden"
                                     pose={openDescription ? 'open' : 'closed'}>
                                <CardText>
                                    <div className="content-wrapper"><p className="text-center"><Translate
                                        place={"footerPages"} string={"Search_description"}/></p></div>
                                </CardText>
                            </Content>

                            <CardTitle className="mb-4">
                                <h5 className="title text-center" style={{cursor: "pointer"}}
                                    onClick={this.toggleOpenReserve}>
                                    <Translate place={"footerPages"} string={"Boats_title"}/>
                                    {!openReserve ? ' ▼ ' : ' ▲ '}
                                </h5>
                            </CardTitle>

                            <Content className="content overflow-hidden" pose={openReserve ? 'open' : 'closed'}>
                                <CardText>
                                    <div className="content-wrapper"><p className="text-center"><Translate
                                        place={"footerPages"} string={"Boats_available"}/></p></div>
                                </CardText>
                            </Content>
                            <CardTitle>
                                <h5 className="title text-center mb-4" style={{cursor: "pointer"}}
                                    onClick={this.toggleOpenProfile}><Translate place={"footerPages"}
                                                                                string={"Review Profile"}/>
                                    {!openProfile ? ' ▼ ' : ' ▲ '}
                                </h5>

                            </CardTitle>
                            <Content className="content overflow-hidden" pose={openProfile ? 'open' : 'closed'}>
                                <CardText>
                                    <div className="content-wrapper"><p className="text-center"><Translate
                                        place={"footerPages"} string={"Profile_description"}/><a href="/perfil"><Translate
                                        place={"footerPages"} string={"Click_here"}/></a></p></div>
                                </CardText>
                            </Content>


                            <CardTitle>
                                <h5 className="title text-center mb-4" style={{cursor: "pointer"}}
                                    onClick={this.toggleOpenBoat}><Translate place={"footerPages"}
                                                                                string={"How_up_boat_title"}/>
                                    {!openBoat ? ' ▼ ' : ' ▲ '}
                                </h5>

                            </CardTitle>
                            <Content className="content overflow-hidden" pose={openBoat ? 'open' : 'closed'}>
                                <CardText>
                                    <div className="content-wrapper"><p className="text-center"><Translate
                                        place={"footerPages"} string={"Boat_description"}/><AltaVaixell/></p></div>
                                </CardText>
                            </Content>




                        </CardBody>

                    </Card>

                </div>
            </Overbody>


            </Body>


        );
    }
}

export default Ajuda;