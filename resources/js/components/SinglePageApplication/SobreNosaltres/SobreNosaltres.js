import React, { Component } from "react";
import Overbody from "../../Overbody/Overbody";
import Body from "../../Body/Body";
import Image from "react-bootstrap/es/Image";
import Translate from "../../../contexts/locale/Translate";


class SobreNosaltres extends Component {
    render() {
        return (
            <Body>
            <Overbody>
            <div>
                <h3 className="text-center"><Translate place={"footerPages"} string={"Who are we?"}/></h3>


                <p className="pl-3">
                        <Translate place={"footerPages"} string={"Mooring Rental Service is a company..."}/>
                    </p>

            </div>
            </Overbody>
            </Body>
        );
    }
}

export default SobreNosaltres;