import React, { Component } from "react";
import {
    Route,
    NavLink,
    HashRouter
} from "react-router-dom";
import Contacte from "./Contacte/Contacte";
import Ajuda from "./Ajuda/Ajuda";
import SobreNosaltres from "./SobreNosaltres/SobreNosaltres";
import Body from './../Body/Body';
import Overbody from "../Overbody/Overbody";
import Translate from "../../contexts/locale/Translate";

class SinglePageApplication extends Component {
    render() {
        return (
            <HashRouter>

            <Body>
                <h1 className="text-center"><Translate place={"footerPages"} string={"Support"}/></h1>
                <Overbody>
                    <div className="w-100 d-flex justify-content-around bg-primary rounded py-3">
                    <NavLink className="text-white-50" to="/Ajuda"><Translate place={"footerPages"} string={"Help"}/></NavLink>
                    <NavLink className="text-white-50" to="/Contacte"><Translate place={"footerPages"} string={"Contact"}/></NavLink>
                    <NavLink className="text-white-50" to="/SobreNosaltres"><Translate place={"footerPages"} string={"About Us"}/></NavLink>
                    </div>
                </Overbody>

                {/*<div className="content">*/}

                    <Route exact path="/" component={Ajuda}/>
                    <Route path="/Ajuda" component={Ajuda}/>
                    <Route path="/Contacte" component={Contacte}/>
                    <Route path="/SobreNosaltres" component={SobreNosaltres}/>

                {/*</div>*/}
            </Body>

            </HashRouter>
        );
    }
}

export default SinglePageApplication;