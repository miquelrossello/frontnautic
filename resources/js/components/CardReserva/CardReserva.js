import React, {Component} from 'react';
import axios from 'axios';
import ImgPort from "../ImgPort/ImgPort";
import Translate from "../../contexts/locale/Translate";

class CardReserva extends Component {

    state = {
        foto: null,
        reserva: null,
        amarrament : null
    };

    componentWillMount() {
        axios.get('/api/reserves/' + this.props.reserva).then(res =>
            this.setState({reserva: res.data})
        ).then(() =>
            axios.get('/api/amarraments/' + this.state.reserva.idAmarrament + '/foto').then(foto =>
                this.setState({foto: foto.data})
            )
        ).then(() =>
            axios.get('/api/amarraments/' + this.state.reserva.idAmarrament).then(amarrament =>
                this.setState({amarrament : amarrament.data})
            )
        );
    }

    render() {
        return (
                <div className="col">
                    {(this.state.reserva !== null && this.state.amarrament !== null) ?
                            <div className="card">
                                <div className="card-header p-0">
                                    <ImgPort img={this.state.foto}/>
                                </div>
                                <div className="card-body">
                                    <div className="row">
                                        <div className="col-sm-12 col-12">
                                            <h4 className="font-weight-bold">{this.state.amarrament.nomAmarrament}</h4>
                                        </div>
                                    </div>
                                    <div className="row mt-2">
                                        <div className="col-sm-12 col-12">
                                            <p>{this.state.reserva.data_reserva}</p>
                                        </div>
                                    </div>
                                    <div className="row mt-2">
                                        <div className="col-sm-6 col-6">
                                            <p>{this.state.reserva.data_inici}</p>
                                        </div>
                                        <div className="col-sm-6 col-6">
                                            <p>{this.state.reserva.data_fi}</p>
                                        </div>
                                    </div>
                                </div>
                            </div> : '' }
                </div>

        )
    }
}

export default CardReserva;