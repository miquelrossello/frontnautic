import React, {Component} from 'react';
import axios from "axios";

class Select extends Component {



    render() {

        return (
            <select className="custom-select" name={this.props.nom} id={this.props.id}>
                {this.props.children}
            </select>
        );
    }
}

export default Select;