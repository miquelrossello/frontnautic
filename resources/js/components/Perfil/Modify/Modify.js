import React, {Component} from 'react';
import posed from 'react-pose';


const MoveImg = posed.img({
    hoverable: true,
    init: {scale: 1},
    hover: {scale: 1.2}
});


class Modify extends Component {

    state = {
        edit: true,
        text: ""
    };

    componentWillMount() {
        const {info} = this.props;
        this.setState({text: info})
    }

    change = (event) => {
        this.setState({text: event.target.value})
    };
    toggleEdit = () => {
        this.setState({edit: !this.state.edit})
    };
    acceptNew = () => {
        const {change} = this.props;
        const {tth} = this.props;
        change(tth, this.state.text);
        this.toggleEdit();
    };
    declineNew = () => {
        const {info} = this.props;
        this.setState({text: info});
        this.toggleEdit();
    };


    render() {

        const {info} = this.props;
        const {change} = this.props;

        return (
            this.state.edit ?
                <div className="d-flex align-items-center justify-content-between border-bottom border-info">
                    <h3 className="text-dark m-0">
                        {info}
                    </h3>
                    <MoveImg onClick={this.toggleEdit} width="25px" height="25px" src="/images/system/edit.svg"
                             alt="edit"/>
                </div>
                :
                <div className="d-flex align-items-center border-bottom border-info">
                    <input onChange={this.change} type="text"
                           className="border-0 w-100 bg-transparent text-capitalize lead" value={this.state.text}/>
                    <MoveImg className="mx-1" onClick={this.acceptNew} width="25px" height="25px"
                             src="/images/system/tick.svg"
                             alt="edit"/>
                    <MoveImg onClick={this.declineNew} width="25px" height="25px" src="/images/system/close.svg"
                             alt="edit"/>
                </div>
        );

    }
}

export default Modify;
