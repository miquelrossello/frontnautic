import React, {Component} from 'react';
import Calendar from 'react-calendar'


class Modify extends Component {

    state = {
        date: new Date()
    };

    componentWillMount() {
        const {date} = this.props;
        const splitDate = date.split("-");
        const newDate = new Date(splitDate[0], splitDate[1] - 1, splitDate[2]);
        this.setState({date: newDate})
    }


    changeDate = (date) => {
        const {change} = this.props;
        const {tth} = this.props;
        this.setState({
                date: date
            }
        );
        const newdate = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
        change(tth, newdate);

    };

    render() {

        const {date} = this.props;
        const {change} = this.props;

        return (
            <Calendar
                className="w-100 bg-transparent border-0"
                onChange={this.changeDate}
                value={this.state.date}
            />
        );

    }
}

export default Modify;
