import React, {Component} from 'react';
import axios from "axios";
import Overbody from './../../Overbody/Overbody'
import BoatAccordion from './../../BoatAccordion/BoatAccordion'
import Translate from "./../../../contexts/locale/Translate";


class Boats extends Component {

    state = {
        boats: null,
        open: null
    };


    componentDidMount() {
        const {user} = this.props;
        axios.get('/api/clients/' + user.idPersona + '/vaixells').then(vaixells => {
            if (vaixells.data !== undefined) {
                if(vaixells.data.length > 0){
                    this.setState({boats: vaixells.data});
                    console.log(vaixells.data)
                }
            }
        });
    }

    toggleOpen(index) {
        this.setState({open: this.state.open === index ? null : index});
    }

    render() {

        return (
            <Overbody>
                <div className="d-flex w-100 flex-column align-items-center pt-3 pb-5">
                    <h1><Translate place={"user"} string={"boats"}/></h1>
                    <div className="w-100 row">
                        <div className="col-6 d-none d-md-flex justify-content-center">
                            <img className="position-absolute" height="100%"
                                 src="/images/system/boatDecoration2.png" alt=""/>
                        </div>
                        <div className="col-12 col-md-6">
                            {this.state.boats !== null
                                ?
                                <div className="w-100 d-flex justify-content-around flex-wrap">
                                    {this.state.boats.map( (boat, index) =>
                                        <BoatAccordion key={index} isOpen={this.state.open === index}
                                                       toggleOpen={this.toggleOpen.bind(this, index)} boat={boat}
                                        />
                                    )}
                                </div>
                                :
                                <div className="w-100 d-flex justify-content-center align-items-center" style={{height: "200px"}}>
                                    No dispones de barcos. Dispones de un boton en la parte superior para introducir un barco
                                </div>
                            }
                        </div>
                    </div>
                </div>
            </Overbody>
        );
    }
}

export default Boats;