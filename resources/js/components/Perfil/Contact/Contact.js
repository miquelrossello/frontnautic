import React, {Component} from 'react';
import {UserContext} from '../../../contexts/User/UserContext';
import Translate from "./../../../contexts/locale/Translate";
import Overbody from './../../Overbody/Overbody'
import Modify from './../../Perfil/Modify/Modify'


class Contact extends Component {

    render() {

        return (
            <UserContext.Consumer>
                {value =>
                    <Overbody>
                        <div className="d-flex w-100 flex-column align-items-center pt-3 pb-5">
                            <h1><Translate place={"user"} string={"contact"}/></h1>
                            <div className="w-100 row">
                                <div className="col-12 col-md-6">
                                    <div className="text-secondary mb-3">
                                        <Translate place={"user"} string={"email"}/>:
                                        <Modify info={value.user.correu} change={value.method} tth={"correu"}/>
                                    </div>
                                    <div className="text-secondary">
                                        <Translate place={"user"} string={"phone"}/>:
                                        <Modify info={value.user.telefon} change={value.method} tth={"telefon"}/>
                                    </div>
                                </div>
                                <div className="col-6 d-none d-md-flex justify-content-center">
                                    <img className="position-absolute" height="100%"
                                         src="/images/system/boatDecoration.png" alt=""/>
                                </div>
                            </div>
                        </div>
                    </Overbody>

                }
            </UserContext.Consumer>
        );
    }
}

export default Contact;
