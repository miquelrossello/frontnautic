import React, {Component} from 'react';
import {UserContext} from '../../../contexts/User/UserContext';
import Translate from "./../../../contexts/locale/Translate";
import CalendarPerfil from './../../Perfil/CalendarPerfil/CalendarPerfil'
import Overbody from './../../Overbody/Overbody'
import Modify from './../../Perfil/Modify/Modify'


class Personal extends Component {

    render() {

        return (
            <UserContext.Consumer>
                {value =>
                    <Overbody>
                        <div className="d-flex w-100 flex-column align-items-center pt-3 pb-5">
                            <div className="w-100 row">
                                <div className="col-12 col-md-6">
                                    <div className="text-secondary mb-3">
                                        <Translate place={"user"} string={"name"}/>:
                                        <Modify info={value.user.nom} change={value.method} tth={"nom"}/>
                                    </div>
                                    <div className="text-secondary mb-3">
                                        <Translate place={"user"} string={"surname"}/>:
                                        <Modify info={value.user.llinatge_1} change={value.method}
                                                tth={"llinatge_1"}/>
                                    </div>
                                    <div className="text-secondary mb-3">
                                        <Translate place={"user"} string={"second_surname"}/>:
                                        <Modify info={value.user.llinatge_2} change={value.method}
                                                tth={"llinatge_2"}/>
                                    </div>
                                    <div className="text-secondary mb-3">
                                        DNI:
                                        <Modify info={value.user.dni} change={value.method} tth={"dni"}/>
                                    </div>
                                </div>
                                <div className="col-12 col-md-6 text-secondary">
                                    <Translate place={"user"} string={"birth"}/>:
                                    <CalendarPerfil date={value.user.data_naixament} change={value.method}
                                                    tth={"data_naixament"}/>
                                </div>
                            </div>
                        </div>
                    </Overbody>

                }
            </UserContext.Consumer>
        );
    }
}

export default Personal;
