import React, {Component} from 'react';
import axios from 'axios';
import Body from './Body/Body'
import Footer from './Footer/Footer'
import Overbody from "./Overbody/Overbody";
import Waiting from './Waiting/Waiting';
import ImgPort from "./ImgPort/ImgPort";
import OverImage from "./OverImage/OverImage";
import OverbodyLight from "./Overbody/OverbodyLight";
import PortMap from './PortMap/PortMap';
import InternalServices from "./InternalServices/InternalServices";
import ExternalServices from "./ExternalServices/ExternalServices";
import Translate from "../contexts/locale/Translate";
import OverbodyDual from "./Overbody/OverbodyDual"

class Port extends Component {

    state = {
        port: null,
        localitat: null
    };

    componentWillMount() {
        let idPort = document.querySelector('meta[name=idPort]').getAttribute('content');
        axios.get('/api/ports/' + idPort)
            .then(res => {
                this.setState({port: res.data});
            });
        axios.get('/api/ports/' + idPort + '/localitat')
            .then(res => {
                this.setState({localitat: res.data});
            });
    }

    render() {

        const {user} = this.props;
        const {locale} = this.props;

        return (
            <div>
                {console.log("value de locale: " + locale)}
                <Body>
                <OverbodyLight>
                    {this.state.port !== null ? (
                            <ImgPort img={this.state.port.foto}>
                                <OverImage dark={true}>
                                    <h1>{this.state.port.nom}</h1>
                                </OverImage>
                            </ImgPort>)
                        : (
                            <Waiting size="100"/>
                        )}
                </OverbodyLight>
                <Overbody>
                    <div className="row w-100">
                        <div className="col-sm-12 p-0 rounded" style={{height: 300 + 'px'}}>
                            {this.state.port !== null ?
                                <PortMap id={this.state.port.idPort}/>
                                : <Waiting size="50"/>}
                        </div>
                    </div>
                </Overbody>
                <OverbodyDual siteL={
                    <div className="w-100">
                        <h2 className="m-2"><Translate place={"port"} string={"title_internal_service"}/></h2>
                        <div className="d-flex align-items-center">
                            <InternalServices isPort={true} lang={locale}/>
                        </div>
                    </div>
                } siteR={
                    <ul className="list-group list-group-flush w-100 rounded">
                        <li className="list-group-item p-0">
                            <h2 className="m-2"><Translate place={"port"} string={"title_contact"}/></h2>
                        </li>
                        <li className="list-group-item ml-2">
                            {this.state.port !== null ? (
                                <p className="m-0"><Translate place={"port"}
                                                              string={"email"}/>: {this.state.port.correu}</p>
                            ) : (
                                <Waiting size="20"/>
                            )}
                        </li>
                        <li className="list-group-item ml-2">
                            {this.state.port !== null ? (
                                <p className="m-0"><Translate place={"port"}
                                                              string={"phone"}/>: {this.state.port.telefon}</p>
                            ) : (
                                <Waiting size="20"/>
                            )}
                        </li>
                        <li className="list-group-item ml-2">
                            {this.state.port !== null ? (
                                <p className="m-0"><Translate place={"port"}
                                                              string={"web"}/>: {this.state.port.web}
                                </p>
                            ) : (
                                <Waiting size="20"/>
                            )}
                        </li>
                        <li className="list-group-item ml-2">
                            {this.state.localitat !== null ? (
                                <p className="m-0"><Translate place={"port"}
                                                              string={"city"}/>: {this.state.localitat.nom}</p>
                            ) : (
                                <Waiting size="20"/>
                            )}
                        </li>
                    </ul>
                }/>
                <Overbody>
                    <div className="container-fluid p-0">
                        <div className="row w-100">
                            <div className="col-sm-12 m-2">
                                <h2><Translate place={"port"} string={"title_external_service"}/></h2>
                            </div>
                        </div>
                        <div className="row w-100 p-2">
                            <div className="col-sm-12">
                                <ExternalServices lang={locale}/>
                            </div>
                        </div>
                    </div>
                </Overbody>
                <Footer/>
                </Body>
            </div>
        );
    }
}

export default Port;