import React, {Component} from 'react';
import Translate from "../contexts/locale/Translate";
import Banner from './Banner/Banner';
import Body from './Body/Body'
import Overbody from './Overbody/Overbody'
import OverbodyLight from './Overbody/OverbodyLight'
import PortMap from './PortMap/PortMap'
import SearchBox from './SearchBox/SearchBox'
import BestPort from './BestPort/BestPort'
import Footer from './Footer/Footer'
import Line from './Line/Line'


class Home extends Component {

    render() {
        const {user} = this.props;
        const {locale} = this.props;
        return (
            <div>
                <Banner>
                    <Body>
                    <OverbodyLight>
                        <SearchBox/>
                    </OverbodyLight>
                    </Body>
                </Banner>
                <Body up={true}>
                <Overbody>
                    <div className="w-100 d-flex flex-column">
                        {user !== "" ?
                            <div>
                                <h4 className="text-center my-3"><Translate place={"home"} string={"hello"}/> {user.nom}</h4>
                                <Line size={"80%"}/>
                                <h4 className="text-center mt-1"><Translate place={"home"} string={"suggestion"}/></h4>
                            </div>
                            :
                            <h4 className="text-center mt-1"><Translate place={"home"} string={"question"}/></h4>
                        }
                        <PortMap size={400}/>
                    </div>
                </Overbody>
                <Overbody zoom={true}>
                        <BestPort/>
                </Overbody>
                <Footer/>
                </Body>
            </div>
        );
    }
}

export default Home;
