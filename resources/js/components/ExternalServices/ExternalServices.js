import React, {Component} from 'react';
import axios from 'axios';
import Waiting from '../Waiting/Waiting';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Service from "../Service/Service";

class ExternalServices extends Component{

    constructor(props) {
        super(props);
        this.state = {
            services: null,
        }
    }

    componentWillMount() {
        let idPort = document.querySelector("meta[name=idPort]").getAttribute('content');
        axios.get("/api/ports/" + idPort + "/tipus_serveis_externs").then(response => {
            this.setState({services: response.data});
        });
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps['lang'] !== this.props.lang) {
            let idPort = document.querySelector("meta[name=idPort]").getAttribute('content');
            axios.get("/api/ports/" + idPort + "/tipus_serveis_externs").then(response => {
                this.setState({services: response.data});
            });
        }
    }

    render() {
        return (
            this.state.services === null ? <Waiting size="20"/> :
            this.state.services[this.props.lang].map((type_services, index) =>
                <div key={index}>
                    <div className="row w-100">
                        <div className="col-sm-4 col-12 m-3">
                            <Service icona={type_services.logo} text={type_services.title} />
                        </div>
                    </div>
                    <div className="row w-100">
                            {type_services.services.map(service =>
                                    <div key={service.idServeiExtern} className="col-sm-4">
                                        <div  className="card h-100">
                                            <div className="card-header">

                                            </div>
                                            <div className="card-body">
                                                <h3><a href={service.web}>{service.nom}</a></h3>
                                                <p>{service.correu}</p>
                                                <p>{service.telefon}</p>
                                            </div>
                                        </div>
                                    </div>
                            )}
                    </div>
                </div>
            )
        )
    }
}

export default ExternalServices;