import React, {Component} from 'react';
import './waiting.css'

class BestPort extends Component {

    static defaultProps = {
        size: 30
    };

    style = {

    };

    render() {

        const {size} = this.props;

        return (
            <img id="waiting" width={size + "px"} height={size + "px"} src="/images/system/waiting.svg" alt="Esperando Amarres"/>
        );
    }
}

export default BestPort;