import React, {Component} from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

class Social extends Component {

    render() {

        const size = "2x";

        return (
            <div className="d-flex flex-row px-3">


                <a className="mx-1" href="https://www.facebook.com/Mooring-Rental-Service-1385046234971903/">
                    <FontAwesomeIcon icon={['fab', 'facebook']} size={size}/>
                </a>

                <a className="mx-1" href="https://www.instagram.com/mooringrentals/">
                    <FontAwesomeIcon icon={['fab', 'instagram']} size={size}/>
                </a>

                <a className="mx-1" href="http://www.twitter.com/MooringRental">
                    <FontAwesomeIcon icon={['fab', 'twitter']} size={size}/>
                </a>

            </div>
        );

    }

}

export default Social;