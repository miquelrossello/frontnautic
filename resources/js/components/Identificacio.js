import React, {Component} from 'react';
import Footer from './Footer/Footer'
import axios from "axios";
import {TabContent, TabPane, Nav, NavItem, NavLink, FormGroup, Row, Col, ModalBody, ModalFooter} from 'reactstrap';
import Inputs from './Inputs/Inputs';
import Labels from "./Labels/Labels";
import Translate from "../contexts/locale/Translate";
import classnames from 'classnames';
import MyButton from "./MyButton/MyButton";
import SelectDinamics from "./SelectDinamics/SelectDinamics";


class Identificacio extends Component {

    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            activeTab: '1'
        };
    }

    toggle(tab) {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }
    render() {

        let token= document.querySelector("meta[name=csrf-token]").getAttribute('content');

        return (
            <div>
                <Row>
                    <Col sm="4"></Col>
                    <Col sm="6">
                <Nav tabs className="mt-5">
                    <NavItem>
                        <NavLink
                            className={classnames({ active: this.state.activeTab === '1' })}
                            onClick={() => { this.toggle('1'); }}
                        >
                            <b><Translate place="navbar" string="login"/></b>
                        </NavLink>
                    </NavItem>
                    <NavItem>
                        <NavLink
                            className={classnames({ active: this.state.activeTab === '2' })}
                            onClick={() => { this.toggle('2'); }}
                        >
                            <b><Translate place="navbar" string="sign_up"/></b>
                        </NavLink>
                    </NavItem>
                </Nav>
                    </Col>
                </Row>
                <TabContent activeTab={this.state.activeTab}>
                    <TabPane tabId="1">
                        <Row>
                            <Col sm="3"></Col>
                            <Col sm="6">
                                <h1 className="mt-3 text-center"><Translate place="navbar" string="login"/></h1>
                            </Col>
                        </Row>

                        <form method="POST" action="/login">
                            <input type="hidden" name="_token" value={token} />
                                <FormGroup>
                                    <Row>
                                        <Col sm="4"></Col>
                                        <Col sm="4">
                                            <Labels id="username" place="login" text="username"/>
                                            <Inputs tipo="text" name="username" id="username"/>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col sm="4"></Col>
                                        <Col sm="4">
                                            <Labels id="password" nom="Password" place="login" text="password"/>
                                            <Inputs tipo="password" name="password" id="password"/>
                                        </Col>
                                    </Row>
                                </FormGroup>
                            <Row>

                                <Col sm="6"></Col>
                                <Col sm="2">
                                    <MyButton tipo="submit" place='navbar' text="login" bloc="btn-block btn-secondary"></MyButton>
                                </Col>
                            </Row>
                        </form>
                    </TabPane>
                    <TabPane tabId="2">
                        <Row>
                            <Col sm="3"></Col>
                            <Col sm="6">
                                <h1 className="mt-3 text-center"><Translate place="navbar" string="sign_up"/></h1>
                            </Col>
                        </Row>
                        <form method="POST" action="/register">
                            <input type="hidden" name="_token" value={token} />
                                <FormGroup>
                                    <Row>
                                        <Col sm="2"></Col>
                                        <Col sm="8">
                                        <Row>
                                        <Col sm="4">
                                            <Labels id="nom" place="register" text="name"/>
                                            <Inputs tipo="text" name="nom" id="nom"/>
                                        </Col>
                                        <Col sm="4">
                                            <Labels id="llinatge_1" place="register" text="lastname1"/>
                                            <Inputs tipo="text" name="llinatge_1" id="llinatge_1"/>
                                        </Col>
                                        <Col sm="4">
                                            <Labels id="llinatge_2" place="register" text="lastname2"/>
                                            <Inputs tipo="text" name="llinatge_2" id="llinatge_2"/>
                                        </Col>
                                        </Row>
                                        </Col>
                                        <Col sm="2"></Col>
                                    </Row>
                                    <Row>
                                        <Col sm="2"></Col>
                                        <Col sm="4">
                                            <Labels id="dni" place="register" text="dni"/>
                                            <Inputs tipo="text" name="dni" id="dni"/>
                                        </Col>
                                        <Col sm="4">
                                            <Labels id="data_naixament" place="register" text="born"/>
                                            <Inputs tipo="text" name="data_naixament" id="data_naixament" placeholder="YYYY-MM-DD"/>
                                        </Col>
                                        <Col sm="2"></Col>
                                    </Row>
                                    <Row>
                                        <Col sm="2"></Col>
                                        <Col sm="8">
                                            <Row>
                                        <Col sm="12">
                                            <Labels id="genere" place="register" text="genere"/>
                                        </Col>
                                        <Col sm="1">
                                            <Inputs clase="ml-2" tipo="radio" name="genere" id="male" value="Masculi"/>
                                        </Col>
                                        <Col sm="1">
                                            <Translate place="register" string="male"/>
                                        </Col>
                                        <Col sm="2"></Col>
                                        <Col sm="1">
                                            <Inputs clase="ml-2" tipo="radio" name="genere" id="female" value="Femeni"/>
                                        </Col>
                                        <Col sm="1">
                                            <Translate place="register" string="female"/>
                                        </Col>
                                        <Col sm="2"></Col>
                                        <Col sm="1">
                                            <Inputs clase="ml-2" tipo="radio" name="genere" id="other" value="Altres"/>
                                        </Col>
                                        <Col sm="1">
                                            <Translate place="register" string="other"/>
                                        </Col>
                                            </Row>
                                        </Col>
                                        <Col sm="2"></Col>
                                    </Row>
                                    <Row className="mt-3">
                                        <Col sm="2"></Col>
                                        <Col sm="8">
                                            <SelectDinamics/>
                                        </Col>
                                        <Col sm="2"></Col>
                                    </Row>
                                    <Row>
                                        <Col sm="2"></Col>
                                        <Col sm="4">
                                            <Labels id="direccio" place="register" text="adresa"/>
                                            <Inputs tipo="text" name="direccio" id="direccio"/>
                                        </Col>
                                        <Col sm="4">
                                            <Labels id="telefon" place="register" text="telefon"/>
                                            <Inputs tipo="text" name="telefon" id="telefon"/>
                                        </Col>
                                        <Col sm="2"></Col>
                                    </Row>
                                    <Row>
                                        <Col sm="2"></Col>
                                        <Col sm="8">
                                            <Labels id="correu" place="register" text="correu"/>
                                            <Inputs tipo="text" name="correu" id="correu"/>
                                        </Col>
                                        <Col sm="2"></Col>
                                    </Row>
                                    <Row>
                                        <Col sm="2"></Col>
                                        <Col sm="8">
                                            <Labels id="username" place="register" text="username"/>
                                            <Inputs tipo="text" name="username" id="username"/>
                                        </Col>
                                        <Col sm="2"></Col>
                                    </Row>
                                    <Row>
                                        <Col sm="2"></Col>
                                        <Col sm="4">
                                            <Labels id="password" place="register" text="password"/>
                                            <Inputs tipo="password" name="password" id="password"/>
                                        </Col>
                                        <Col sm="4">
                                            <Labels id="password_confirmation" place="register" text="password"/>
                                            <Inputs tipo="password" name="password_confirmation" id="password_confirmation"/>
                                        </Col>
                                        <Col sm="2"></Col>
                                    </Row>
                                    <Row>
                                        <Col sm="2"></Col>
                                        <Col sm="8">
                                            <div className="ml-4 mt-3">
                                            <Inputs tipo="checkbox" name="dades" id="dades"/><a href="#"><Translate place="register" string="proteccio"/></a>
                                            </div>
                                        </Col>
                                        <Col sm="2"></Col>
                                    </Row>
                                </FormGroup>
                            <Row>
                                <Col sm="3"></Col>
                                <Col sm="4"></Col>
                                <Col sm="3">
                                    <MyButton tipo="submit" place='button' text="register" bloc="btn-block mt-5 btn-secondary"></MyButton>
                                </Col>
                            </Row>
                        </form>
                    </TabPane>
                </TabContent>
                <Footer/>
            </div>
        );
    }

}

export default Identificacio;