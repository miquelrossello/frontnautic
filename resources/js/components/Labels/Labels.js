import React, { Component } from 'react';
import { Label } from 'reactstrap';
import Translate from './../../contexts/locale/Translate';

class Labels extends Component {

    static defaultProps = {
        id:"",
        nom:"Input"

    }

    render() {
        return (

                <Label className="mt-2" for={this.props.id}><Translate place={this.props.place} string={this.props.text}/></Label>


        );

    }

}
export default Labels;