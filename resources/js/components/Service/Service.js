import React, { Component } from 'react';
import {  Row, Col } from 'reactstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

class Service extends Component {

    static defaultProps = {
       size: "2x",
       text:"",
       icona:"wifi"
    }

    render() {
        return (
            <Row className="m-2">
                <Col className="col-6 col-sm-6">
                    <FontAwesomeIcon icon={this.props.icona} size={this.props.size} />
                </Col>
                <Col className="col-6 col-sm-6 p-0">
                    <b>{this.props.text}</b>
                </Col>

            </Row>
        );

    }

}
export default Service;

