import React, { Component } from 'react';
import {Row, Col} from 'reactstrap';
import Service from "../Service/Service";
import axios from "axios";
import Waiting from '../Waiting/Waiting';

class InternalServices extends Component {

    state = {
        serveis: null,
        isPort: false
    };

    componentWillMount() {
        (this.props.isPort) ? this.setState({isPort : true}) : this.setState({isPort : false});
        if (this.props.isPort === true) {
            let idPort = document.querySelector("meta[name=idPort]").getAttribute('content');
            axios.get("/api/ports/" + idPort + "/serveis_interns").then(response => {
                this.setState({serveis: response.data});
            })
        } else {
            let idZona= document.querySelector("meta[name=id-zona]").getAttribute('content');
            axios.get("/api/zones/"+idZona+"/serveis_interns").then(response =>{
                this.setState({serveis:response.data})

            })
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps['lang'] !== this.props.lang) {
            if (this.state.isPort === true) {
                let idPort = document.querySelector("meta[name=idPort").getAttribute('content');
                axios.get("/api/ports/" + idPort + "/serveis_interns").then(response => {
                    this.setState({serveis: response.data});
                })
            } else {
                let idZona= document.querySelector("meta[name=id-zona]").getAttribute('content');
                axios.get("/api/zones/" + idZona + "/serveis_interns").then(response =>{
                    this.setState({serveis:response.data})

                })
            }
        }
    }


    render() {
        return (
            <Row>
                <Col sm="12">
                    <Row>
                        {(this.state.serveis !== null) ?
                            this.state.serveis[this.props.lang].map(serv =>
                                <Col xs="6" sm="6" key={serv.idServeiIntern}>
                                    <Service icona={serv.icona} text={serv.nom}/>
                                </Col>) : <Waiting size="50"/>
                        }
                    </Row>
                </Col>
            </Row>
        );

    }

}
export default InternalServices;
