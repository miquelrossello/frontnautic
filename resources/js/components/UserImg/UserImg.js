import React, {Component} from 'react';
import './userImg.css'


class UserImg extends Component {

    static defaultProps = {
        img: "default.png",
        size: 100
    };

    render() {

        let {img} = this.props;
        const {size} = this.props;

        if(img === null || img === ""){
            img = UserImg.defaultProps.img
        }

        const style = {
            backgroundImage: 'url("/images/profile/' + img + '")',
            height: size + "px",
            width: size + "px",
        };

        return (
            <div id="userImg" style={style} className="rounded-circle">
                {this.props.children}
            </div>
        );

    }
}

export default UserImg;
