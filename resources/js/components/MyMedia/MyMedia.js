import React, { Component } from 'react';
import {Media,Col,Row} from "reactstrap";
import Translate from './../../contexts/locale/Translate';


class MyMedia extends Component {



    render() {
        return (

            <Media>
                <Media left href="#">
                    <Media object data-src="holder.js/64x64" alt="Generic placeholder image" />
                </Media>
                <Media body>
                    <Media heading>
                        Nom Port
                    </Media>
                     <Row>
                         <Col sm="3">
                             Desde:1234-5-6
                         </Col>
                         <Col sm="3">
                             Fins:1234-5-13
                         </Col>
                     </Row>
                </Media>
            </Media>

        );

    }
}

export default MyMedia;