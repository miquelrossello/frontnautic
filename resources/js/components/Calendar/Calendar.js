import React, {Component} from 'react';
import FullCalendar from 'fullcalendar-reactwrapper';
import 'fullcalendar-reactwrapper/dist/css/fullcalendar.min.css';

class Calendar extends Component {
    render() {
        return (
            <FullCalendar header = {this.props.header}
                          events={this.props.events} />
        );
    }
}

export default Calendar;