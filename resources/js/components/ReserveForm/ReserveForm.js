import React, {Component} from 'react';
import Input from "reactstrap/es/Input";
import Label from "reactstrap/es/Label";
import axios from "axios";
import './reserveForm.css';
import ImgPort from "../ImgPort/ImgPort";
import OverImage from "../OverImage/OverImage";
import Waiting from "../Waiting/Waiting";
import OverbodyLight from "../Overbody/OverbodyLight";
import Body from "../Body/Body";


class ReserveForm extends Component {


    render() {

        const {amarre} = this.props;

        return (

            <Body>

            <div className="">

                <h4>Amarrament #{amarre !== null ? amarre.numeroAmarrament : ''}</h4>
                <h5>Fondària {amarre !=null ? amarre.fondaria: ''}</h5>
                <h5>Llargària {amarre !=null ? amarre.llargaria: ''}</h5>
                <h5>Amplada {amarre !=null ? amarre.amplitud: ''}</h5>


            </div>

            </Body>
        );
    }
}

export default ReserveForm;