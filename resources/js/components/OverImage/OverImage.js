import React, {Component} from 'react';
import './overImage.css'

class OverImage extends Component {

    static defaultProps = {
        dark: true,
        pos: "center"
    };

    render() {
        const {dark} = this.props;
        //acepta 3 pos (center, start, end)
        const {pos} = this.props;

        return (
            <div className={"d-flex align-items-" + pos + " rounded w-100 h-100 fullOverDark"} >
                <div className={"w-100 text-center p-2 rounded m-2 " + (dark ? "overLightImage" : "overDarkImage")}>
                    {this.props.children}
                </div>
            </div>
        );
    }
}

export default OverImage;