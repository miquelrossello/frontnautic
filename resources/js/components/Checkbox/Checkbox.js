import React, {Component} from 'react';

class Checkbox extends Component {
    render() {
        return (

            <div className="custom-control custom-checkbox">

                <input className="custom-control-input" type="checkbox" name="" id="chk1"/>

                <label htmlFor="chk1" className="custom-control-label">Default checked</label>

            </div>

        )
            ;
    }
}

export default Checkbox;