import React, {Component} from 'react';
import ReserveForm from './ReserveForm/ReserveForm';
import Footer from './Footer/Footer';
import Body from "./Body/Body";
import axios from "axios";
import Overbody from "./Overbody/Overbody";
import OverbodyLight from "./Overbody/OverbodyLight";
import ImgPort from "./ImgPort/ImgPort";
import Waiting from "./Waiting/Waiting";
import Translate from "../contexts/locale/Translate";
import Button from "./Button/Button";
import Payment from "./Payment/Payment";

class Reserva_Amarrament extends Component {

    constructor(props) {
        super(props);
        this.doReservation = this.doReservation.bind(this);
    }

    state = {
        amarrament: null,
        vaixells: null,
        dateIni: null,
        dateEnd: null,
        prices: null,
    };

    doReservation() {
        let data = {};
        let curDate = new Date();
        //Selected Boat can be undefined. Should we control that?
        if (this.state.boat_option !== undefined && this.state.dateIni !== null
            && this.state.dateEnd !== null && this.state.amarrament !== null && this.state.prices !== null) {
            if (this.state.boat_option === "selected" && this.state.selected_boat === undefined) {
                console.log("Can't do the reservation!");
            } else {
                data = {
                    data_inici : this.state.dateIni,
                    data_fi : this.state.dateEnd,
                    data_reserva : curDate.getFullYear() + '-' + (curDate.getMonth() + 1) + '-' + curDate.getDate(),
                    preu_total : this.state.prices.total,
                    comissio : this.state.prices.comissions,
                    idAmarrament : this.state.amarrament.idAmarrament,
                    idPoliticaCancelacio : this.state.amarrament.idPoliticaCancelacio,
                    idClient : this.state.client.data,
                    idMetodePagament : null
                };
                axios.post('/api/reserves', data).then(res =>
                    this.setState({reservation : res.data})
                ).then(() =>
                    (this.state.boat_option === 'selected' && this.state.selected_boat !== undefined) ?
                        axios.post('/api/reserves/' + this.state.reservation.idReserva + '/vaixells', {matricula : this.state.selected_boat})
                        : console.log('Boat not setted'),
                    err => console.log(err)
                ).finally(() =>
                    self.location.href = '/reserves/' + this.state.reservation.idReserva
                );
            }
        } else {
            console.log("Can't do the reservation!");
        }
    }

    handleRadio(event) {
        this.setState({[event.target.name] : event.target.value});
    }

    handleSelect(event) {
        this.setState({[event.target.name] : event.target.options[event.target.selectedIndex].value});
    }

    componentWillMount() {
        let idAmarrament = document.querySelector('meta[name=idAmarrament]').getAttribute('content');
        axios.get('/api/amarraments/' + idAmarrament).then(res =>
                this.setState({amarrament : res.data})
        );
        axios.get('/api/session/client').then(idClient =>
            axios.get('/api/clients/' + idClient.data + '/vaixells').then(res =>
                this.setState({vaixells : res.data, client : idClient})
            )
        );
        axios.all([
            axios.get('/api/session/dataIni'),
            axios.get('/api/session/dataEnd')
        ]).then(axios.spread((dataIni, dataEnd) =>
            this.setState({
                dateIni : dataIni.data,
                dateEnd : dataEnd.data
            })
        )).then(() =>
            axios.get('/api/amarraments/' + idAmarrament + '/price').then(res =>
                this.setState({prices : res.data})
            )
        );
    }

    render() {
        const {user} = this.props;
        const {locale} = this.props;
        return (
            <div>
                <Body>
                    <Overbody>
                        {this.state.amarrament === null ? <Waiting size="100"/> :
                           <div className="w-100">
                                <div className="row">
                                    <div className="col-sm-12">
                                        <div className="card w-100">
                                            <div className="card-img-top">
                                                <ImgPort img={this.state.amarrament.foto}/>
                                            </div>
                                            <div className="card-body">
                                                <h1 className="card-title text-center">{this.state.amarrament.nom}<span className="text-muted"> {"#" + this.state.amarrament.numeroAmarrament}</span></h1>
                                            </div>
                                                <div className="row m-0">
                                                    <div className="col-sm-6 col-12">
                                                        <div className="row">
                                                            <div className="col-sm-12">
                                                                <h2><Translate place="reservaAmarrament" string="title_boat_option"/></h2>
                                                                <div className="form-check">
                                                                    <input className="form-check-input" type="radio" name="boat_option" id="boat_option_unselected" value="unselected" onClick={this.handleRadio.bind(this)}/>
                                                                    <label className="form-check-label" htmlFor="boat_option_unselected"><Translate place="reservaAmarrament" string="boat_option_unselected"/></label>
                                                                </div>
                                                                <div className="form-check">
                                                                    {this.state.vaixells !== null ?
                                                                        <div>
                                                                            <input className="form-check-input" type="radio" name="boat_option" id="boat_option_selected" value="selected" onClick={this.handleRadio.bind(this)} />
                                                                            <label className="form-check-label" htmlFor="boat_option_selected"><Translate place="reservaAmarrament" string="boat_option_selected"/></label>
                                                                            <select className="custom-select mt-2" name="selected_boat" onChange={this.handleSelect.bind(this)}>
                                                                                <option value="null">...</option>
                                                                                {this.state.vaixells.map(data =>
                                                                                    <option key={data.matricula} value={data.matricula}>{data.nom}</option>
                                                                                )}
                                                                            </select>
                                                                        </div>
                                                                        :
                                                                        <Button text={<Translate place="reservaAmarrament" string="new_boat"/>}/>
                                                                    }
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="row mt-3">
                                                            <div className="col-sm-12">
                                                                <h2><Translate place="reservaAmarrament" string="title_payment"/></h2>
                                                                <Payment/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div className="col-sm-6 col-12 bg-light rounded mt-sm-0 mt-2">
                                                        <div className="row">
                                                            <div className="col-sm-12">
                                                                <h2><Translate place="reservaAmarrament" string="title_reservation_data"/></h2>
                                                                <div className="row">
                                                                    <div className="col-sm-1"></div>
                                                                    <div className="col-sm-10">
                                                                        <hr />
                                                                    </div>
                                                                    <div className="col-sm-1"></div>
                                                                </div>
                                                                <div className="row mt-2 p-2">
                                                                    <div className="col-sm-6 col-6">
                                                                        <h5 className="m-0 text-center"><Translate place="reservaAmarrament" string="start_date"/>: {this.state.dateIni !== null ? this.state.dateIni : <Waiting size="15"/>}</h5>
                                                                    </div>
                                                                    <div className="col-sm-6 col-6">
                                                                        <h5 className="m-0 text-center"><Translate place="reservaAmarrament" string="end_date"/>: {this.state.dateEnd !== null ? this.state.dateEnd : <Waiting size="15"/>}</h5>
                                                                    </div>
                                                                </div>
                                                                <div className="row">
                                                                    <div className="col-sm-1"></div>
                                                                    <div className="col-sm-10">
                                                                        <hr />
                                                                    </div>
                                                                    <div className="col-sm-1"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div className="row mt-3">
                                                            <div className="col-sm-12">
                                                                <h2><Translate place="reservaAmarrament" string="mooring_data"/></h2>
                                                                <table className="table table-borderless table-secondary">
                                                                    <thead className="w-100">
                                                                        <tr>
                                                                            <td>
                                                                                <b><Translate place="infoAmarrament" string="fondaria"/></b>
                                                                            </td>
                                                                            <td>
                                                                                <b><Translate place="infoAmarrament" string="llargaria"/></b>
                                                                            </td>
                                                                            <td>
                                                                                <b><Translate place="infoAmarrament" string="amplada"/></b>
                                                                            </td>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody className="w-100">
                                                                        <tr>
                                                                            <td>
                                                                                {this.state.amarrament.fondaria} <Translate place="system" string="meters"/>
                                                                            </td>
                                                                            <td>
                                                                                {this.state.amarrament.llargaria} <Translate place="system" string="meters"/>
                                                                            </td>
                                                                            <td>
                                                                                {this.state.amarrament.amplitud} <Translate place="system" string="meters"/>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <div className="row mt-3">
                                                            <div className="col-sm-12">
                                                                <h2><Translate place="reservaAmarrament" string="reserva_price"/></h2>
                                                                <table className="table table-borderless table-success">
                                                                    <thead className="w-100">
                                                                    <tr>
                                                                        <td>
                                                                            <b><Translate place="payment" string="base_price"/></b>
                                                                        </td>
                                                                        <td>
                                                                            <b><Translate place="payment" string="comissions"/></b>
                                                                        </td>
                                                                        <td>
                                                                            <b><Translate place="payment" string="total_price"/></b>
                                                                        </td>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody className="w-100">
                                                                    <tr>
                                                                        <td>{this.state.dateIni !== null ? this.state.dateIni : <Waiting size="15"/>} | {this.state.dateEnd !== null ? this.state.dateEnd : <Waiting size="15"/>}</td>
                                                                        <td>{this.state.prices !== null ? this.state.prices.comissions : <Waiting size='15'/>} € (5%)</td>
                                                                        <td>{this.state.prices !== null ? this.state.prices.total : <Waiting size='15'/>} €</td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <div className="row m-4 d-flex justify-content-center">
                                                            <div className="col-sm-8 col-12">
                                                                <Button onClick={this.doReservation} extraStyles="w-100" text={<Translate place="reservaAmarrament" string="btn_do_reservation"/>}/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                           </div>}
                    </Overbody>
                </Body>
                <Footer/>
            </div>

        );

    }

}

export default Reserva_Amarrament;

