import React, {Component} from 'react';
import {Map, Marker, Popup, TileLayer, Tooltip} from 'react-leaflet';
import Waiting from './../Waiting/Waiting'
import 'leaflet/dist/leaflet.css';
import axios from "axios";

class PortMap extends Component {

    state = {
        ports: []
    };

    static defaultProps = {
        id: null,
        size: 300,
        zoom: 8
    };


    componentWillMount() {
        const {id} = this.props;
        if (id !== null) {
            if (Array.isArray(id)) {
                id.map(num => {
                        this.getPort(num)
                    }
                )
            } else {
                this.getPort(id)
            }


        } else {
            this.getAllPorts();
        }
    }

    getPort = (id) => {
        axios.get("/api/ports/" + id).then(response => {
            if (response.data !== undefined) {
                const ports = this.state.ports;
                ports.push(response.data);
                this.setState({ports: ports});
            }
        })
    };

    getAllPorts = () => {
        axios.get("/api/ports").then(response => {
            if (response.data[0] !== undefined) {
                this.setState({ports: response.data});
            }
        })
    };


    render() {

        const {size} = this.props;
        const {zoom} = this.props;
        const ports = this.state.ports;
        const icon = L.icon({
            iconUrl: '/images/system/anchor.svg',
            iconSize: [40, 30],
        });

        return (
            ports.length > 0 ? (
                <Map center={[ports[0].latitud, ports[0].longitud]} zoom={zoom} className="w-100 bg-transparent"
                     style={{height: (size + "px")}}>
                    <TileLayer attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                               url="https://api.mapbox.com/styles/v1/mapbox/streets-v9/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoicWNqMjg2MTciLCJhIjoiY2p0NWgxa2Z3MDV4ajQ0bXcxZzVsNHhyeiJ9.9Ur3i1ASnWO3f9D8p976-A
"/>
                    {ports.map((port, index) =>
                        <Marker key={index} onClick={function () {
                            location.href = "/ports/" + port.idPort;
                        }} position={[port.latitud, port.longitud]} icon={icon}>
                            <Tooltip direction='top' opacity={0.7}>
                                <span> {port.nom} </span>
                            </Tooltip>
                        </Marker>
                    )}
                </Map>
            ) : (
                <Waiting size={150}/>
            )
        );
    }
}

export default PortMap;