import React, {Component} from 'react';
import Body from "./../Body/Body";

class OverbodyDual extends Component {

    static defaultProps = {
        inCenter: true,
        zoom: false
    };

    render() {

        const {inCenter} = this.props;
        const {zoom} = this.props;
        const {siteL} = this.props;
        const {siteR} = this.props;

        return (
            <div className="row my-4 d-flex justify-content-center">
                <div
                    className={"col-11 col-md-4 p-0 bg-white my-2 overflow-hidden shadow-lg rounded d-flex justify-content-center align-items-center " + (inCenter ? "d-flex justify-content-center align-items-center " : "") + (zoom ? "zoomXS" : "")}>
                    {siteL}
                </div>
                <div className="d-none d-sm-block col-md-1">
                </div>
                <div
                    className={"col-11 col-md-4 p-0 bg-white my-2 overflow-hidden shadow-lg rounded d-flex justify-content-center align-items-center " + (inCenter ? "d-flex justify-content-center align-items-center " : "") + (zoom ? "zoomXS" : "")}>
                    {siteR}
                </div>

            </div>
        );
    }
}

export default OverbodyDual;