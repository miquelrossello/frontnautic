import React, {Component} from 'react';

class OverbodyLight extends Component {

    render() {

        return (
            <div className="row my-4 d-flex justify-content-center">
                <div className="col-11 col-md-9 p-0 bg-light overflow-hidden rounded d-flex justify-content-center align-items-center">
                    {this.props.children}
                </div>
            </div>
        );
    }
}

export default OverbodyLight;