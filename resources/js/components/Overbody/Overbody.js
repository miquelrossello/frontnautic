import React, {Component} from 'react';

class Overbody extends Component {

    static defaultProps = {
        inCenter: true,
        zoom: false
    };

    render() {

        const {inCenter} = this.props;
        const {zoom} = this.props;

        return (
            <div className="row my-4 d-flex justify-content-center">
                <div className={"col-11 col-md-9 p-0 overflow-hidden bg-white shadow-lg rounded " + (inCenter ? "d-flex justify-content-center align-items-center " : "") + (zoom ? "zoomXS" : "")}>
                    {this.props.children}
                </div>
            </div>
        );
    }
}

export default Overbody;