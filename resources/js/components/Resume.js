import React, {Component} from 'react';
import Body from "./Body/Body";
import Overbody from "./Overbody/Overbody";
import Translate from "../contexts/locale/Translate"
import axios from 'axios';
import Waiting from './Waiting/Waiting';
import ImgPort from "./ImgPort/ImgPort";

class Resume extends Component {

    state = {
        reserva : null,
        amarrament : null,
        propietari : null,
        img_port : null,
        vaixells : null
    };

    componentWillMount() {
        let idReserva = document.querySelector('meta[name=idReserva]').getAttribute('content');
        axios.get('/api/reserves/' + idReserva).then(res =>
            this.setState({reserva : res.data})
        ).then(() =>
            axios.all([
                axios.get('/api/reserves/' + idReserva + '/amarrament'),
                axios.get('/api/reserves/' + idReserva + '/propietari'),
                axios.get('/api/reserves/' + idReserva + '/vaixells')]
            ).then(axios.spread((amarrament, propietari, vaixells) =>
                this.setState({
                    amarrament : amarrament.data,
                    propietari : propietari.data,
                    vaixells : (vaixells.data === "") ? null : vaixells.data
                })
            ))
        ).finally(() =>
            axios.get('/api/amarraments/' + this.state.amarrament.idAmarrament + '/foto').then(foto =>
                this.setState({img_port : foto.data})
            )
        );
    }

    render() {
        return (
            <Body>
                <Overbody>
                    <div className="row w-100">
                        <div className="col-sm-12 col-12">
                            <div className="row">
                                <div className="col-sm-12 col-12">
                                    <h2 className="text-center"><Translate place="resume" string="title_resume"/> <span className="text-muted">{(this.state.amarrament !== null ) ? this.state.amarrament.nomAmarrament + ' #' + this.state.reserva.idReserva : <Waiting size="10" />}</span></h2>
                                </div>
                            </div>
                            <div className="row mt-2">
                                <div className="col-sm-12 col-12">
                                    <p className="text-center text-muted"><Translate place="resume" string="reservation_date"/>: {(this.state.reserva !== null) ? this.state.reserva.data_reserva : <Waiting size="15"/>}</p>
                                </div>
                            </div>
                            <div className="row mt-2 mb-4">
                                <div className="col-sm-6 col-12">
                                    <div className="row">
                                        <div className="col-sm-12 col-12">
                                            <div className="card">
                                                <div className="card-header">
                                                    <h2 className="m-0"><Translate place="resume" string="mooring_title"/></h2>
                                                </div>
                                                <div className="card-body p-0">
                                                    <div className="row">
                                                        <div className="col-sm-12 col-12">
                                                            <div className="row">
                                                                <div className="col-sm-12 col-12">
                                                                    <ImgPort img={this.state.img_port}/>
                                                                </div>
                                                            </div>
                                                            <div className="row mt-2 p-2">
                                                                <div className="col-sm-12 col-12">
                                                                    <h5 className="font-weight-bold"><Translate place="resume" string="measures"/></h5>
                                                                    <table className="table">
                                                                        <thead>
                                                                        <tr>
                                                                            <td className="font-weight-bold">
                                                                                <Translate place="infoAmarrament" string="amplada"/>
                                                                            </td>
                                                                            <td className="font-weight-bold">
                                                                                <Translate place="infoAmarrament" string="fondaria"/>
                                                                            </td>
                                                                            <td className="font-weight-bold">
                                                                                <Translate place="infoAmarrament" string="llargaria"/>
                                                                            </td>
                                                                        </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                        <tr>
                                                                            <td>{(this.state.amarrament !== null) ? this.state.amarrament.amplitud: <Waiting size="20"/>} <Translate place="system" string="meters"/></td>
                                                                            <td>{(this.state.amarrament !== null) ? this.state.amarrament.fondaria: <Waiting size="20"/>} <Translate place="system" string="meters"/></td>
                                                                            <td>{(this.state.amarrament !== null) ? this.state.amarrament.llargaria: <Waiting size="20"/>} <Translate place="system" string="meters"/></td>
                                                                        </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row mt-4">
                                        <div className="col-sm-12 col-12 mt-sm-0 mt-4">
                                            <div className="card">
                                                <div className="card-header">
                                                    <h2 className="m-0"><Translate place="resume" string="info_owner_title"/></h2>
                                                </div>
                                                <div className="card-body">
                                                    <div className="row">
                                                        <div className="col-sm-3 col-6">
                                                            <h6 className="font-weight-bold"><Translate place="resume" string="owner_name"/></h6>
                                                            <p>{(this.state.propietari !== null) ? this.state.propietari.nom : <Waiting size="20"/>}</p>
                                                        </div>
                                                        <div className="col-sm-3 col-6">
                                                            <h6 className="font-weight-bold"><Translate place="resume" string="owner_second_names"/></h6>
                                                            <p>{(this.state.propietari !== null) ? this.state.propietari.llinatge_1 + ' ' + this.state.propietari.llinatge_2 : <Waiting size="20"/>}</p>
                                                        </div>
                                                        <div className="col-sm-3 col-6">
                                                            <h6 className="font-weight-bold"><Translate place="port" string="phone"/></h6>
                                                            <p className="text-muted">{(this.state.propietari !== null) ? this.state.propietari.telefon : <Waiting size="15"/>}</p>
                                                        </div>
                                                        <div className="col-sm-3 col-6">
                                                            <h6 className="font-weight-bold"><Translate place="port" string="email"/></h6>
                                                            <p className="text-muted">{(this.state.propietari !== null) ? this.state.propietari.correu : <Waiting size="15"/>}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {(this.state.vaixells !== null) ?
                                        this.state.vaixells.map(vaixell =>
                                            <div className="row mt-4" key={vaixell.matricula}>
                                                <div className="col-sm-12 col-12 mt-sm-0 mt-4">
                                                    <div className="card">
                                                        <div className="card-header">
                                                            <h2><Translate place="resume" string="boat_info_title"/></h2>
                                                        </div>
                                                        <div className="card-body">
                                                            <div className="row">
                                                                <div className="col-sm-4 col-12">
                                                                    <h5 className="font-weight-bold"><Translate place="vaixell" string="nom"/></h5>
                                                                    <p className="text-muted">{vaixell.nom}</p>
                                                                </div>
                                                                <div className="col-sm-4 col-12">
                                                                    <h5 className="font-weight-bold"><Translate place="vaixell" string="matricula"/></h5>
                                                                    <p className="text-muted">{vaixell.matricula}</p>
                                                                </div>
                                                            </div>
                                                            <div className="row mt-2">
                                                                <div className="col-sm-4 col-6">
                                                                    <h6 className="font-weight-bold"><Translate place="vaixell" string="eslora"/></h6>
                                                                    <p className="text-muted">{vaixell.eslora} <Translate place="system" string="meters"/></p>
                                                                </div>
                                                                <div className="col-sm-4 col-6">
                                                                    <h6 className="font-weight-bold"><Translate place="vaixell" string="maniga"/></h6>
                                                                    <p className="text-muted">{vaixell.maniga} <Translate place="system" string="meters"/></p>
                                                                </div>
                                                                <div className="col-sm-4 col-12">
                                                                    <h6 className="font-weight-bold"><Translate place="vaixell" string="calat"/></h6>
                                                                    <p className="text-muted">{vaixell.calat} <Translate place="system" string="meters"/></p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        ) : ''}
                                </div>
                                <div className="col-sm-6 col-12 mt-sm-0 mt-4">
                                    <div className="card">
                                        <div className="card-header">
                                            <h2 className="card-title m-0"><Translate place="resume" string="prices"/></h2>
                                        </div>
                                        <div className="card-body">
                                            <table className="table">
                                                <tbody>
                                                    <tr>
                                                        <td className="font-weight-bold">
                                                            <Translate place="payment" string="comissions"/>
                                                        </td>
                                                        <td>{(this.state.reserva !== null) ? this.state.reserva.comissio : <Waiting size="10"/>} €</td>
                                                    </tr>
                                                    <tr>
                                                        <td className="font-weight-bold">
                                                            <Translate place="payment" string="total_price"/>
                                                        </td>
                                                        <td>{(this.state.reserva !== null) ? this.state.reserva.preu_total: <Waiting size="10"/>} €</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </Overbody>
            </Body>
        );
    }
}

export default Resume;