import React, {Component} from 'react';
import Footer from './Footer/Footer'
import Body from './Body/Body';
import Overbody from './Overbody/Overbody'
import SearchBox from './SearchBox/SearchBox';
import MyCard from './MyCard/MyCard';

class Search extends Component {

    render() {
        const {user} = this.props;
        const {locale} = this.props;
        return (
            <div>

                <Body>
                <Overbody>
                    <SearchBox/>
                </Overbody>
                <Overbody>
                    <MyCard/>
                </Overbody>
                </Body>


                <Footer/>
            </div>
        );
    }
}

export default Search;