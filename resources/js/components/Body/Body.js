import React, {Component} from "react";


class Body extends Component {

    static defaultProps = {
        up: false
    };

    render() {

        const {up} = this.props;

        if (up) {
            const style = {
                top: "90vh",
                zIndex: 10
            };
            return (
                <div style={style} className="container-fluid position-absolute">
                    {this.props.children}
                </div>
            );
        } else {
            return (
                <div className="container-fluid">
                    {this.props.children}
                </div>
            );
        }
    }
}

export default Body;