import React, { Component } from 'react';
import {Input} from 'reactstrap';
import Translate from './../../contexts/locale/Translate';

class Inputs extends Component {

    static defaultProps = {
        value:"",
        name: "",
        id:"",
        place: "",
        text:"",
        placeholder: ""
    }

    render() {
        return (
            <Input className={this.props.clase} type={this.props.tipo} name={this.props.name} id={this.props.id} defaultValue={this.props.value} placeholder={this.props.placeholder}/>
        );

    }

}
export default Inputs;