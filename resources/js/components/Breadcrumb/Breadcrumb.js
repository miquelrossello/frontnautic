import React, {Component} from 'react';

class Breadcrumb extends Component {
    render() {
        return (
        <div>

            <nav aria-label="breadcrumb">
                <ol className="breadcrumb">
                    <li className="breadcrumb-item active" aria-current="page">Inicio</li>
                </ol>
            </nav>

            <nav aria-label="breadcrumb">
                <ol className="breadcrumb">
                    <li className="breadcrumb-item"><a href="#">Inicio</a></li>
                    <li className="breadcrumb-item active" aria-current="page">Perfil Cliente</li>
                </ol>
            </nav>

            <nav aria-label="breadcrumb">
                <ol className="breadcrumb">
                    <li className="breadcrumb-item"><a href="#">Inicio</a></li>
                    <li className="breadcrumb-item"><a href="#">Perfil Cliente</a></li>
                    <li className="breadcrumb-item active" aria-current="page">Ficha Amarre</li>
                </ol>
            </nav>

        </div>

        )
            ;
    }
}

export default Breadcrumb;