import React, {Component} from 'react';
import './alerts.css';

class Alerts extends Component {

    static defaultProps = {

        state: 2


    };


    render() {

        const {state} = this.props;


        if (state == 0) {

            return (

                <div className="alert bg-primary" role="alert">
                    This is a primary alert—check it out!
                </div>


            );

        } else if (state == 1) {

            return (

                <div className="alert bg-success" role="alert">
                    Correcto!
                </div>


            );

        } else {

            return (

                <div className="alert bg-danger" role="alert">
                    Error!
                </div>


            );

        }


    }


}

export default Alerts;