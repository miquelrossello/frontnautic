import React, {Component} from 'react';
import Translate from './../../contexts/locale/Translate'
import {library} from '@fortawesome/fontawesome-svg-core';
import {fas} from '@fortawesome/free-solid-svg-icons';
import {fab} from '@fortawesome/free-brands-svg-icons';
import Social from './../Social/Social'
import Line from './../Line/Line'
import './footer.css'

library.add(fas, fab);

class Footer extends Component {
    render() {

        return (
            <footer className="w-100 bg-transparent mt-5">
                <Line size="90%"/>
                <div className="container-fluid h-100">
                    <div className="row h-100 px-3 d-flex justify-content-center align-items-center">
                        <div className="h-50 col-6 order-1 col-md-2 d-flex justify-content-md-center justify-content-start align-items-center p-0 m-0">
                            <a href="/footer#/SobreNosaltres">¿M.R.S.?</a>
                        </div>
                        <div className="h-50 col-6 mr-auto order-2 col-md-3 d-flex justify-content-md-center justify-content-end align-items-center p-0 m-0">
                            <a href="/footer#/Ajuda"><Translate place={"footer"} string={'how_it_works?'}/></a>
                        </div>
                        <div className="h-50 col-6 ml-auto order-3 col-md-3 d-flex justify-content-md-center justify-content-start align-items-center p-0 m-0">
                            <a href="/footer#/Contacte"><Translate place={"footer"} string={'how_contact-us?'}/></a>
                        </div>
                        <div className="h-50 col-6 order-4 col-md-2 d-flex justify-content-md-center justify-content-end align-items-center p-0 m-0">
                            <Social/>
                        </div>


                        <div className="position-absolute d-flex py-0">
                            <div className="d-flex justify-content-center align-items-center rounded-circle bg-primary m-0 py-3 shadow zoomXL"
                                id="logoFooter">
                                <img className="h-100" src="/images/system/logo-white.svg" alt="logo mooring rental service"/>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        );
    }
}

export default Footer;