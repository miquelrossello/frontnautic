import React, {Component} from 'react';

class Button extends Component {

    static defaultProps = {
        type : "btn-primary",
        extraStyles : ""
    };

    render() {
        return (
            <button className={'btn ' + this.props.type + ' ' + this.props.extraStyles} onClick={this.props.onClick}>{this.props.text}</button>
        );
    }
}

export default Button;