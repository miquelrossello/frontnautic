import React, {Component} from 'react';
import Translate from "../../contexts/locale/Translate";
import axios from "axios";
import Waiting from './../Waiting/Waiting'
import OverImage from './../OverImage/OverImage'

class BestPort extends Component {

    state = {
        bestPort: null
    };

    componentWillMount() {
        axios.get("/api/ports/best").then(response => {
            if (response.data[0] !== undefined) {
                this.setState({bestPort: response.data[0]});
            }
        })
    }

    render() {

        let style;
        if (this.state.bestPort !== null) {
            style = {
                background: 'url("/images/port/' + this.state.bestPort.foto + '") center center no-repeat',
                backgroundSize: "cover",
                height: "300px"
            };
        }

        return (
            this.state.bestPort !== null ? (
                <div style={style} className="w-100 rounded ali">
                    <a className="h-100 w-100" href={"/ports/" + this.state.bestPort.idPort}>
                        <OverImage dark={true}>
                            <h1>{this.state.bestPort.nom}</h1>
                            <h5><Translate place={"home"} string={"bestPort"}/> {this.state.bestPort.numRes} <Translate place={"system"} string={"reservations"}/></h5>
                        </OverImage>
                    </a>
                </div>
            ) : (
                <Waiting size={150}/>
            )


        );
    }
}

export default BestPort;