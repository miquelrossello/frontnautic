import React, {Component} from 'react';
import Footer from './Footer/Footer'
import axios from "axios";
import ImgPort from "./ImgPort/ImgPort";
import Overbody from "./Overbody/Overbody";
import Body from "./Body/Body";
import InfoAmarrament from "./InfoAmarrament/InfoAmarrament";
import OverImage from "./OverImage/OverImage";
import InternalServices from "./InternalServices/InternalServices";
import Waiting from './Waiting/Waiting';
import {Col, Row} from 'reactstrap';
import Translate from "../contexts/locale/Translate";

class Detall_amarrament extends Component {


    state = {
        amarrament: {}
    };

    componentWillMount() {
        let id = document.querySelector("meta[name=id-amarrament]").getAttribute('content');
        axios.get("/api/amarraments/" + id).then(response => {
            this.setState({amarrament: response.data})
        })
    };

    render() {
        const {user} = this.props;
        const {locale} = this.props;


        return (
            this.state.amarrament !== null ?
                <div>
                    <Body>
                    <Overbody>
                        <ImgPort img={this.state.amarrament.foto}>
                            <OverImage dark={true} pos="end">
                                <h1>{this.state.amarrament.nom}</h1>
                            </OverImage>
                        </ImgPort>
                    </Overbody>
                    <Overbody>
                        <InfoAmarrament
                            id={this.state.amarrament.idAmarrament}
                            llarg={this.state.amarrament.llargaria}
                            ample={this.state.amarrament.amplitud}
                            fons={this.state.amarrament.fondaria}
                            puntuacio={this.state.amarrament.puntuacio_mitja}
                        />
                    </Overbody>
                    <Overbody>
                        <Row>
                            <Col sm="12">
                                <h3 className="text-center mb-3 mt-2"><b><Translate place={"MyCard"} string={"services"}/></b></h3>
                            </Col>
                            <Col>
                                <InternalServices isPort={false} lang={locale}/>
                            </Col>
                        </Row>
                    </Overbody>
                    </Body>
                    <Footer/>
                </div>
                : <Waiting size="50"/>

        );
    }


}

export default Detall_amarrament;