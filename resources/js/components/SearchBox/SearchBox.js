import React, {Component} from 'react';
import Calendar from 'react-calendar'
import posed from 'react-pose';
import Autosuggest from './Autosuggest/Autosuggest'
import axios from "axios";
import Translate from "../../contexts/locale/Translate";


const Parent = posed.div({
    open: {
        applyAtStart: {display: "block"},
        height: "100%",
        beforeChildren: true,
    },
    close: {
        height: "0px",
        afterChildren: true,
        applyAtEnd: {display: "none"},
    },
});
const Children = posed.div({
    open: {
        opacity: 1,
    },
    close: {
        opacity: 0,
    },
});


class SearchBox extends Component {

    state = {
        dateIni: new Date(),
        dateEnd: new Date(),
        isVisibleIni: false,
        isVisibleEnd: false,
        suggestions: null,
        suggestionAccept: null,
        text: ""
    };

    componentWillMount() {
        const enddate = this.state.dateEnd;
        enddate.setDate(this.state.dateEnd.getDate() + 1);
        this.setState({dateEnd: enddate});
        axios.get('/api/session').then(ses => {
            if (ses.data.dataIni !== "" && ses.data.dataIni !== undefined) {
                const sDate = ses.data.dataIni;
                const splitDate = sDate.split("-");
                const date = new Date(splitDate[0], splitDate[1] - 1, splitDate[2]);
                this.setState({dateIni: date})
            }
            if (ses.data.dataEnd !== "" && ses.data.dataEnd !== undefined) {
                const sDate = ses.data.dataEnd;
                const splitDate = sDate.split("-");
                const date = new Date(splitDate[0], splitDate[1] - 1, splitDate[2]);
                this.setState({dateEnd: date})
            }
            if (ses.data.place !== "" && ses.data.place !== undefined) {
                this.setState({text: ses.data.place})
            }
            if (ses.data.table !== "" && ses.data.table !== undefined) {
                this.setState({suggestionAccept: {"table": ses.data.table}})
            }
        })
    }


    changeDate = (date) => {
        if (this.state.isVisibleIni) {
            this.setState({
                    dateIni: date
                }
            );
            this.toggleVisibilityEnd();
        } else if (this.state.isVisibleEnd) {
            this.setState({
                    dateEnd: date
                }
            );
            this.toggleVisibilityEnd();
        }
    };
    toggleVisibilityIni = () => {
        this.setState({
                isVisibleIni: !this.state.isVisibleIni,
                isVisibleEnd: false,
                suggestions: null
            }
        )
    };
    changeText = (text) => {
        this.setState({
                text: text
            }
        )
    };
    toggleVisibilityEnd = () => {
        this.setState({
                isVisibleEnd: !this.state.isVisibleEnd,
                isVisibleIni: false,
                suggestions: null
            }
        )
    };
    updateSuggestions = (suggestions) => {
        this.setState({
            suggestions: suggestions,
            isVisibleIni: false,
            isVisibleEnd: false,
            suggestionAccept: null,
        })
    };
    createSuggestions = () => {
        const suggestions = this.state.suggestions;
        const existPais = (suggestions.pais.length > 0);
        const existProv = (suggestions.provincia.length > 0);
        const existLoca = (suggestions.localitat.length > 0);
        const existPort = (suggestions.port.length > 0);

        return (
            <div className="row w-100 m-0 d-flex justify-content-center">
                <div className="col-12 text-center"><Translate place={"home"} string={"where"}/></div>
                {existPais ? (
                    <div className="list-group col-11 col-lg-5 p-0 m-2">
                        <h4 className="list-group-item text-center bg-light p-1 border-bottom-0"><Translate place={"home"} string={"countries"}/></h4>
                        {this.state.suggestions.pais.map((pais, index) =>
                            <button value="pais" id={pais[0]} title={pais[1]} onClick={this.selectSuggestion}
                                    key={index}
                                    className="list-group-item-action btn text-center bg-light">{pais[1]}</button>
                        )}
                    </div>
                ) : ""}
                {existProv ? (
                    <div className="list-group col-11 col-lg-5 p-0 m-2">
                        <h4 className="list-group-item text-center bg-light p-1 border-bottom-0"><Translate place={"home"} string={"municipality"}/></h4>
                        {this.state.suggestions.provincia.map((provincia, index) =>
                            <button value="provincia" id={provincia[0]} title={provincia[1]}
                                    onClick={this.selectSuggestion} key={index}
                                    className="list-group-item-action btn text-center bg-light">{provincia[1]}</button>
                        )}
                    </div>
                ) : ""}
                {existLoca ? (
                    <div className="list-group col-11 col-lg-5 p-0 m-2">
                        <h4 className="list-group-item text-center bg-light p-1 border-bottom-0"><Translate place={"home"} string={"locations"}/></h4>
                        {this.state.suggestions.localitat.map((localitat, index) =>
                            <button value="localitat" id={localitat[0]} title={localitat[1]}
                                    onClick={this.selectSuggestion} key={index}
                                    className="list-group-item-action btn text-center bg-light">{localitat[1]}</button>
                        )}
                    </div>
                ) : ""}
                {existPort ? (
                    <div className="list-group col-11 col-lg-5 p-0 m-2">
                        <h4 className="list-group-item text-center bg-light p-1 border-bottom-0"><Translate place={"home"} string={"ports"}/></h4>
                        {this.state.suggestions.localitat.map((port, index) =>
                            <button value="port" id={port[0]} title={port[1]} onClick={this.selectSuggestion}
                                    key={index}
                                    className="list-group-item-action btn text-center bg-light">{port[1]}</button>
                        )}
                    </div>
                ) : ""}
            </div>
        );
    };
    selectSuggestion = (e) => {
        this.setState({
            suggestionAccept: {"table": e.target.value},
            text: e.target.title,
            suggestions: null
        });
    };

    render() {

        const dateIni = this.state.dateIni.getFullYear() + "-" + (this.state.dateIni.getMonth() + 1) + "-" + this.state.dateIni.getDate();
        const dateEnd = this.state.dateEnd.getFullYear() + "-" + (this.state.dateEnd.getMonth() + 1) + "-" + this.state.dateEnd.getDate();

        return (
            <div className="row w-100">
                <div className="col px-0">
                    <form className="row d-flex justify-content-center mx-1 mx-md-0" action="/search"
                          method="POST">
                        <input type="hidden" name="_method" value="GET"/>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                        <input type="hidden" name="table"
                               value={this.state.suggestionAccept !== null ? this.state.suggestionAccept.table : "port"}/>
                        <Autosuggest updateSuggestions={this.updateSuggestions} text={this.state.text}
                                     changeText={this.changeText}/>
                        <input onClick={this.toggleVisibilityIni} type="text" readOnly={true}
                               className={"form-control form-control-lg col-6 col-sm-5 col-md-2 mx-auto m-1 border shadow-none p-0 text-center " + (this.state.isVisibleIni ? "border-primary" : "")}
                               name="dataIni" value={dateIni}>
                        </input>
                        <input onClick={this.toggleVisibilityEnd} type="text" readOnly={true}
                               className={"form-control form-control-lg col-6 col-sm-5 col-md-2 mx-auto m-1 border shadow-none p-0 text-center " + (this.state.isVisibleEnd ? "border-primary" : "")}
                               name="dataEnd" value={dateEnd}>
                        </input>
                        <button type="submit" style={{zIndex: 1}} disabled={this.state.suggestionAccept === null}
                                className="d-flex form-control-lg btn btn-secondary col-12 col-sm-2 col-md-2 mx-auto m-1 shadow zoomXS justify-content-center align-items-center">
                            <img width="25px" height="25px" src="/images/system/search.svg" alt="busqueda amarres"/>
                        </button>
                    </form>
                    <div className="row m-0">
                        <Parent className="col p-0 m-0"
                                pose={this.state.isVisibleIni || this.state.isVisibleEnd ? 'open' : 'close'}>
                            <Children className="w-100 rounded-bottom">
                                <Calendar
                                    className="w-100 rounded-bottom bg-transparent border-0 position-relative"
                                    onChange={this.changeDate}
                                    value={this.state.isVisibleIni ? this.state.dateIni : this.state.dateEnd}
                                />
                            </Children>
                        </Parent>
                    </div>
                    <div className="row m-0">
                        <Parent className="col p-0 m-0 d-flex justify-content-center"
                                pose={this.state.suggestions !== null ? 'open' : 'close'}>
                            <Children className="row w-100">
                                <div className="col-12 rounded-bottom p-0">
                                    {this.state.suggestions !== null ? this.createSuggestions() :
                                        <div style={{height: "100px"}}>

                                        </div>
                                    }
                                </div>
                            </Children>
                        </Parent>
                    </div>
                </div>
            </div>
        );
    }
}

export default SearchBox;