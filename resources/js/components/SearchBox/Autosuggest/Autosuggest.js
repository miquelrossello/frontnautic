import React, {Component} from 'react';
import axios from "axios";

class Autosuggest extends Component {

    limit = {"pais": 2, "provincia": 10, "localitat": 10, "port": 10};

    state = {
        map: null
    };


    componentDidMount() {
        axios.get("/api/browser").then(response => {
            this.setState({map: response.data});
        });
    }

    render() {

        const map = this.state.map;
        const limit = this.limit;
        const {updateSuggestions} = this.props;
        const {text} = this.props;
        const {changeText} = this.props;

        function searchSuggest(e) {
            changeText(e.target.value);
            const text = e.target.value.toLowerCase().trim();
            const textLen = text.length;
            let places = null;

            if (textLen > 0 && map !== null) {
                places = {"pais": [], "provincia": [], "localitat": [], "port": []};
                for (let i = 0; i < map.length; i++) {
                    let place = map[i];

                    let pPais = [place["idPais"], place["Pais_Nom"]];
                    let pProv = [place["idProvincia"], place["Provincia_Nom"]];
                    pProv.push(pPais);
                    let pLoca = [place["idLocalitat"], place["Localitat_Nom"]];
                    pLoca.push(pProv);
                    let pPort = [place["idPort"], place["Port_Nom"]];
                    pPort.push(pLoca);

                    structure(pPais, places.pais);
                    structure(pProv, places.provincia);
                    structure(pLoca, places.localitat);
                    structure(pPort, places.port);
                }
                const numPais = runTypeOfPlaces(limit.pais, places.pais);
                const numProv = runTypeOfPlaces(limit.provincia, places.provincia);
                const numLoca = runTypeOfPlaces(limit.localitat, places.localitat);
                const numPort = runTypeOfPlaces(limit.port, places.port);

                ((numPais + numProv + numLoca + numPort) === 0) ? (places = null) : null;
            }

            function structure(prototype, capsule) {
                if (prototype[1].toLowerCase().substring(0, textLen) === text) {
                    if (capsule.length <= 0) {
                        capsule.unshift(prototype);
                    } else if (capsule[0][0] !== prototype[0]) {
                        capsule.unshift(prototype);
                    }
                }
            }

            function runTypeOfPlaces(limit, capsule) {
                for (var i = 0; i < (capsule.length - (limit - 1)); i++) {
                    let numRan = Math.floor(Math.random() * (capsule.length));
                    capsule.splice(numRan, 1);
                }
                return capsule.length;
            }

            updateSuggestions(places);
        }

        return (
            <input onChange={searchSuggest} type="text"
                   autoComplete="off"
                   className="form-control form-control-lg col-12 col-md-5 mx-auto m-1 border shadow-none text-center"
                   placeholder="¿Dónde?"
                   value={text}
                   name="place"/>
        )
    }
}

export default Autosuggest;