import React, {Component} from 'react';
import posed from "react-pose";
import Calendar from 'react-calendar'


const Box = posed.div({
    hidden: {height: "0px"},
    visible: {height: "100%"}
});


class TravelData extends Component {

    state = {
        isVisible: true
    };

    changeDataIni = (date) => {
        this.setState({
                isVisible: !this.state.isVisible
            }
        )
    };

    render() {
        return (
            <Calendar
                className="w-100 h-100 rounded-bottom bg-transparent border-0"
                onChange={this.changeDataIni}
                value={this.state.dateIni}
            />
        );
    }
}

export default TravelData;