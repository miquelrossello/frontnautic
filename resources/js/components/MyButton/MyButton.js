import React, { Component } from 'react';
import {Button} from "reactstrap";
import Translate from './../../contexts/locale/Translate';


class MyButton extends Component {
    static defaultProps = {
        color:"primary",
        mida:"",
        bloc:"",
        text:"Enviar",
    };



    render() {
        return (
            <Button onClick={this.props.click}  className={this.props.bloc} color={this.props.color} size={this.props.mida} type={this.props.tipo} >
                <Translate place={this.props.place} string={this.props.text}/>
            </Button>
        );

    }
}

export default MyButton;
