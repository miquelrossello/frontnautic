<!DOCTYPE html>
<html lang="es">
<head>
    <title>Registre</title>
    <meta charset="UTF-8">
    <style>
        .title_text {
            font-size: 32px;
            color: white;
        }

        .logo {
            width: 100px;
            height: 100px;
        }
    </style>
    <link href="{{asset('css/app.css')}}" type="text/css" rel="stylesheet">
</head>
<body>
    <header class="row d-flex align-items-center">
        <div class="col-sm-12">
            <div class="row bg-primary d-flex">
                <div class="col-sm-12">
                    <h1 class="text-center title_text">Mooring Rental Service</h1>
                </div>
                <img src="{{url('/images/system/logo-white.svg')}}" alt="mooring-rental-service" class="m-auto logo">
            </div>
        </div>
    </header>
    <div class="container d-flex">
        <div class="col-sm-12">
            <h1 class="text-center mt-4">Benvingut, {{ $user->nom }} al nostre portal web.</h1>
            <div class="mt-4">
                <p>Per a poder navegar com a usuari dins la nostra web, primer has de fer el procés de verificació del compte.</p>
                <p class="p-3">Per a fer-ho, només has de clickar el següent botó:</p>
                <a class="btn btn-primary mx-auto d-block" href="">Verificar</a>
            </div>
        </div>
    </div>
    <footer class="row mt-5">
        <div class="col-sm-12">
            <h4 class="text-muted text-center">Mooring Rental Service @ 2019</h4>
        </div>
    </footer>
</body>
</html>