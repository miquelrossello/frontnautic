<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Registre</title>
        <link href="{{asset('css/app.css')}}" type="text/css" rel="stylesheet">
        <style>
            .title_text {
                font-size: 32px;
            }

            .logo {
                width: 100px;
                height: 100px;
            }
        </style>
    </head>
    <body>
        @yield('header')
        <div class="row d-flex align-items-center">
            <div class="col-sm-12">
                <div class="row bg-primary d-flex">
                    <div class="col-sm-12">
                        <h1 class="text-center title_text text-white">Mooring Rental Service</h1>
                    </div>
                    <img src="images/system/logo-white.svg" alt="mooring-rental-service" class="m-auto logo">
                </div>

            </div>
        </div>
        @yield('body')

        @yield('footer')
        <footer class="row mt-5">
            <div class="col-sm-12">
                <h4 class="text-muted text-center">Mooring Rental Service @ 2019</h4>
            </div>
        </footer>
    </body>
</html>